(ns user
  (:require
   [clojure.spec.alpha :as spec]
   [wikla.tree :as t]
   [wikla.specs]
   [orchestra.spec.test :refer [instrument unstrument]]))

(comment

  (require '[wikla.tree :as t] :reload-all)

  (require '[wikla.specs])
  
  (instrument)

  (unstrument)

  (load-file "examples/everything.clj")

  (load-file "examples/simple.clj")

  (load-file "examples/basics.cljc")

  (load-file "examples/bench.clj")

  (load-file "examples/lrs.clj")

  )
