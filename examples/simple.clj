;; examples/simple.clj

(ns wikla-simple
  (:require [wikla.tree :as t]))

(def unticked-tree
  (t/root {:properties {:name "Demo!"}
           :tree (t/parallel
                  {:properties {:name "One Shall Stand, One Shall Fall."}
                   :children   [(t/sequence
                                 {:properties {:name "Eager Failure"}
                                  :children   [t/success-condition
                                               t/failure-condition
                                               t/success-condition]})
                                (t/selector
                                 {:properties {:name "Eager Success"}
                                  :children   [t/failure-condition
                                               t/success-condition
                                               t/failure-condition]})]})}))

(t/print-tree unticked-tree)

(def ticked-tree (t/tick unticked-tree))

(println)

(t/print-tree ticked-tree)

;; Save PNG snapshots

(t/->png unticked-tree "s-tree-0.png")
(t/->png ticked-tree "s-tree-1.png")

:done