;; examples/everything.cljc

(ns wikla-full-house
  (:require [wikla.tree :as t]))

(defn breaker [label point]
  (t/action
   {:properties {:name (str "Breaker: " label)}
    :parameters {:pred point}
    :tick-fn    (fn [self]
                  (let [p (get (t/params self) :pred t/no)]
                    (if (p (t/yield self))
                      (t/running self)
                      (t/success self))))}))

(def unticked
  (t/root
   {:properties {:name "Bigger Demo!"}
    :tree (t/parallel
           {:properties {:name "Win, Fail & Rob"}
            :success-th 2
            :failure-th 3
            :cycle-flag t/RUNNING
            :children  [(t/selector
                         {:properties {:name "Winning."}
                          :children   [(t/to-failure identity)
                                       (t/to-success identity)
                                       t/failure-condition]})
                        (t/sequence
                         {:properties {:name "Of Fail."}
                          :children   [(breaker "#1" :breakpoint1)
                                       t/failure-condition
                                       t/success-condition]})
                        (t/robin
                         {:properties {:name "Just Roll."}
                          :retry-flag t/RUNNING
                          :summary-fn (fn [self]
                                        (if (:breakpoint2 (t/yield self))
                                          (t/running self) (t/success self)))
                          :children   [(t/decorator
                                        {:child        t/success-condition
                                         :post-join-fn t/failure})
                                       (t/decorator
                                        {:child t/failure-condition})
                                       t/success-condition]})]})}))

;; Three Ticks

(def once (t/tick (t/yield unticked {:breakpoint1 true :breakpoint2 true})))

(def twice (t/tick (t/yield once (assoc (t/yield once) :breakpoint1 false))))

(def trice (t/tick (t/yield twice (assoc (t/yield twice) :breakpoint2 false))))

;; PNG snapshots

(t/->png unticked "everything-tree-0.png")
(t/->png once "everything-tree-1.png")
(t/->png twice "everything-tree-2.png")
(t/->png trice "everything-tree-3.png")

:done