(ns decorate
  (:require [wikla.tree :as t]))

;; Context Decorator! Transform stash (extract) before child runs,
;; transform it back (merge) after.
;;
;; * requires two functions:
;; `(extract-context decorator) --> decorator'`
;; `(merge-context decorator') --> decorator''`
;; 
;; * initial stash stored in parameters via
;;    `(make-snapshot decorator stash) => decorator'`
;; * initial stash cleared from paramters via
;;    `(unmake-snapshot decorator') => decorator''`
;; * initial stash reachable from paramters via
;;    `(snapshot decorator) => any`
;; * snapshot presence check via
;;    `(has-snapshot? decorator) => Bool
;;
;; Steps, starting as leaf:
;; * tick:
;;   - branch-pred: child is Ready and snapshot not present
;;   - own-tick:
;;     - snapshot stash
;;     - extract-context
;;     - become branch
;;     - signal Ready for child to be picked up
;; * child with altered stash executes and comes back
;; * join:
;;   - post-join
;;     - copy flag from child
;;     - merge-context
;;     - nullify snapshot
;;     - if child is done:
;;       - return
;;     - else (if child is Running):
;;       - become leaf
;;       - make child Ready (for next tick)
;;       - signal inherited Running

(defn snapshot
  ([self]
   (get (t/params self) :stash-snapshot)))

(defn has-snapshot?
  ([self]
   (get (t/params self) :snapshot-set? false)))

(defn make-snapshot
  ([self]
   (t/in-params self assoc :snapshot-set? true
                :stash-snapshot (t/yield self))))

(defn unmake-snapshot
  ([self]
   (t/in-params self assoc :snapshot-set? false
                :stash-snapshot nil)))

(defn enter-ctx
  "Snapshot, extract, become branch, get Ready."
  [self]
  (assert (not (has-snapshot? self)) "Snapshot set in enter-ctx!")
  (let [extract-ctx (-> self :properties :extract-ctx)]
    (-> (make-snapshot self)
        (extract-ctx)
        (assoc :branching? true)
        (t/ready))))

(defn leave-ctx
  "Shift, merge, erase snapshot, if not done - become leaf and make child Ready."
  [self]
  (let [merge-ctx (-> self :properties :merge-ctx)
        self'     (-> (t/shift self (t/shift (get self :child)))
                      (merge-ctx)
                      (unmake-snapshot))]
    (if (t/done? self')
      self'
      (-> (update self' :child t/shift t/READY)
          (assoc :branching? false)))))

(defn alter-stash
  "Decorate to transform stash before and after child runs."
  [extract-ctx merge-ctx child]
  (unmake-snapshot
   (t/decorator
    {:branch-pred  has-snapshot?
     :own-tick-fn  enter-ctx
     :post-join-fn leave-ctx
     :branching?   false
     :properties   {:extract-ctx extract-ctx
                    :merge-ctx   merge-ctx}
     :child        child})))

;; increment :a in stash
(def increment-a
  (t/root-of
   (alter-stash
    #(t/in-stash %1 :a)
    #(t/yield %1 (assoc (snapshot %1) :a (t/yield %1)))
    (t/label (t/to-success inc) "just inc"))))

(def inc-ed (t/tick (t/yield increment-a {:a 100})))

;; on Failure restore snapshot
(def transact
  (t/root-of
   (alter-stash
    identity
    #(if (t/failure? %1) (t/yield %1 (snapshot %1)) %1)
    (t/sequence*
     (t/label (t/to-success inc) "just inc")
     (t/label (t/to-success inc) "just another inc")
     (t/label (t/fn->condition (comp odd? t/yield)) "odd is ok")))))

(def undone-transact (t/tick (t/yield transact 100)))

(def done-transact (t/tick (t/yield transact 99)))