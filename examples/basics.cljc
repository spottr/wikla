;; examples/basics.cljc

(ns wikla-basics
  (:require [wikla.tree :as t]))

(defn tree-printer
  "Show tree down from given node and its stash."
  [node]
  (t/print-tree node)
  (println " =>" (t/yield node))
  (println)
  node)

;; Leaf - a Condition to check if given value is in expected range.
;;
;; Condition implements a tick that delegates to provided pred-fn,
;; boolean returned is turned into condition shift.

(def C
  (t/condition
   {:properties {:name "value-in-range?"}
    :parameters {:lo 0.0 :hi 1.0}
    :pred-fn    (fn value-in-range? [self]
                  (let [{:keys [lo hi] :or {lo 0.0 hi 1.1}} (t/params self)]
                    (<= lo (t/yield self) hi)))}))

;; It's not a branch and it's Ready to go.

(t/branch? C) ;; => false

(t/shift C) ;; => :wikla.core/Ready

;; Can be ticked.

(tree-printer (t/tick (t/yield C 2.2))) ;; Condition [Failure]

(tree-printer (t/tick (t/yield C 0.5))) ;; Condition [Success]

;; Composite - Sequence node that will hold the Condition.
;;             ... and a success-action that only succeeds.

(def S (t/sequence {:children [C t/success-action]}))

;; It is a branch and it is Ready to go.

(t/branch? S) ;; => true

(t/shift S) ;; => :wikla.core/Ready

;; Sequence will present the Condition for execution via part method.

(= (t/part S) C) ;; => true

;; We can now tick the Condition and give it back to the Sequence.

(tree-printer (t/join S (t/tick (t/yield C 2.2)))) ;; => Sequence [Failure]

(tree-printer (t/join S (t/tick (t/yield C 0.5)))) ;; => Sequence [Success]

;; Root that will run the Sequence

(def R (t/root {:tree S}))

(t/branch? R) ;; => false

(t/shift R) ;; => :wikla.core/Ready

;; Ticking the Root.

(tree-printer (t/tick (t/yield R 2.2))) ;; Root [Failure]

(tree-printer (t/tick (t/yield R 0.5))) ;; Root [Success]

;; Leaf - an Action to clamp the value that was not in range.
;;
;; Action implements a tick via delegation to tick-fn.

(def A
  (t/action
   {:properties {:name "Clamp out-of-bound value."}
    :parameters {:lo 0.0 :hi 2.0}
    :tick-fn    (fn [self]
                  (let [{:keys [lo hi] :or {lo 0.0 hi 1.0}} (t/params self)]
                    (t/success self (min hi (max lo (t/yield self))))))}))

(tree-printer (t/tick (t/yield A 0.5))) ;; Action [Success]

(tree-printer (t/tick (t/yield A 2.2))) ;; Action [Success]

;; Composite - a Selector that will check and correct the value.

(def S? (t/selector {:children [C A]}))

;; Composite - a Sequence of that Selector and a success-action

(def S! (t/sequence {:children [S? t/success-action]}))

;; Root that will tick the Sequence over Selector.

(def R? (t/root {:tree S!}))

;; Ticking the root

(tree-printer (t/tick (t/yield R? 0.5))) ;; Root [Success]

(tree-printer (t/tick (t/yield R? 2.2))) ;; Root [Success]

;; Note: yes, this example is contrived and trivial, uses Sequence that
;;       is not needed there and that Action would be fine on itself too,
;;       since it will implicitly do that check.
;;       It's about showing the `how`.

:done