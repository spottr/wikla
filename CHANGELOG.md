# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

This project adheres to [Semantic Versioning](https://semver.org/), with caveat
that below 1.0.0 minor releases will break compatibility.

## Wishlist

## [Unreleased]

## [1.0.3] - 2024-01-27

### Added

* `merge-props` and `merge-params` recursive zippers.
* No-op `nil`/pass-through `params` on `Selector` and `Sequence`.
* `Root`s own `parameters`, since tree might not have them.

## [1.0.2] - 2023-10-19

### Fixed

* `wikla.tree/friendlify` Clojure processing reorder bug fixed.

## [1.0.1] - 2023-10-19

### Fixed

* `wikla.tree/friendlify` Clojure processing reordered.

## [1.0.0] - 2023-06-24

### Changed

* Version 1, API is now stable. Code identical to 0.9.5.

## [0.9.5] - 2022-06-10

### Fixed

* `wikla.tree/friendlify` regexp fixes and Clojure/ClojureScript split.

## [0.9.4] - 2022-06-02

### Added

* `wikla.tree/friendlify` function with spec
* Missing spec for `wikla.tree/in-props` function

### Changed

* If no name given `wikla.tree/fn->action` friendlifies tick-fn name
* If no name given `wikla.tree/fn->condition` friendlifies pred-fn name

## [0.9.3] - 2022-05-15

### Added

* `wikla.tree/props` properties get-set and `wikla.tree/in-props`.

## [0.9.2] - 2022-01-23

### Added

* `wikla.tree/in-params` - convenience parameters update function.
* `wikla.tree/dtx` - execute `Stepper` until exception throwing step.

## [0.9.1] - 2021-12-07
### Added

* `wikla.tree/in-stash` - convenience stash update function.

### Fixed

* Robin `:cycle-flag` and `:retry-flag` now READY like in `Parallel`.
* Nilability removed from node seed specs.

## [0.9.0] - 2021-08-29

* QOL: Introduced ARCHITECTURE document and improved README.
* QOL: Test runner modified to utilize eftest for Clojure portion.
* BREAK: Renamed `wikla.core` to `wikla.tree`.
* BREAK: Removed `wikla.ext`, `wikla.pre`, `wikla.zip`, `wikla.kit`, `wikla.dot`.
* BREAK: Rewrite of `Node` protocol and remove `Status`/`Result`.
* BREAK: Rewrite of `Stepper` protocol and remove of `all-tick`.
* BREAK: Removed `(tick node stash)` arity, as `stash` is part of the node now.
* BREAK: `Condition` semantics changed: predicate over self.
* BREAK: `Selection` is now called `Selector`.
* BREAK: `Parallel` reimplemented, it now runs `RUNNING` children in
  round-robin fashion until all complete, then counts children for thresholds.
  For retry it resets children by default, but allows for custom strategy
  to be plugged in. Cycle completion and retry completion statuses can be
  customized via `:cycle-flag` and `:retry-flag` fields.
* BREAK: `Decorator` reimplemented to not have sub nodes other than the child.
* BREAK: `Ticker` renamed to `Root` and implementing `Stepper` protocol.
  BREAK: `Robin` reimplemented in `wikla.tree` as `Parallel` alternative.
* BREAK: `Parallel` and `Robin` default RUNNING->READY on cycle and retry.
* BREAK: On `join` and `tick` nodes that are `done?` return self.
* BREAK: `part` on `done?` branch it returns `nil`.
* BREAK: Removed `resetable` and `reset`, removed `Rewindable` and `rewind`.
* BREAK: Removed predefined decorators and rest of `wikla.pre`.
* BREAK: `Transaction`, `Unless`, `Until`, `Context` gone with `wikla.ext`.
* BREAK: `Dot` protocol removed, parts incorporated into `Node`.
* BREAK: Dot generation moved to `wikla.tree` namespace.
* BREAK: `Zip` protocol removed, `wikla.tree/zipper` based on `Node`.
* BREAK: All node constructors accept a map in the shape of node.
* BREAK: Removed a lot of bondage in the code: less checks, less
  exceptions, more straight code paths.
* BREAK: Removed Nashorn for cljs testing, nodejs only.

## [0.8.3] - 2021-07-27

* Added `kit/rewind-and-save`: makes rewound nodes resetable.
* Fixed some `wikla.core/branches`: force children into vector.
* Fixed `wikla.ext/Unless` child drop on `s-condition` being false.
* BREAK: removed `wikla.core/return-node` shim.
* BREAK: removed `wikla.kit/node-maker`.
* BREAK: reimplemented Decorator, removed sub-Actions, changing state
  order to add additional READY after join to execute post-join tick.
  The post-join tick does not get fed Result anymore, it runs on the
  Decorator with `:child` updated.
* Fix: Added stashless arity to `wikla.core/post-join-pass-through`.
* Fix: Added stashless arity to `wikla.pre/run-or-default`.
* Improved some Exception messages.
* Improved and extended several tests.
* Made linter a bit happier.

## [0.8.2] - 2021-01-13

### Fixed

* `wikla.pre/bool-to-status` dropped stash on tick. Unintended.

## [0.8.1] - 2021-01-08

### Changed

* QOL: Error reporting: big data structures (tree/stash/nodes) in ex-info
       repacked in callable to clean up Exception display. Get the key, call
       the function it holds to unpack the data structure. Modified
       `wikla.kit/show-trail` to work with that model. Fixed some misnamed
       :keywords in ex-data.

## [0.8.0] - 2020-09-27

### Changed

* Moved dependencies Clojure/ClojureScript from default profile to dev/uberjar.
* Added Clojure 1.10.2-alpha2 profile as 1.10.2.
* BREAK: Taxonomy `Node/-node?` and `Status/-status?` with nil/Object impls.
* BREAK: `(hooks [this] [this new-hooks])` :wikla.core/hooks in Node.
* BREAK: `(stem [this] [this new-node])` to swap :node in Status.
* BREAK: `(yield [this] [this new-stash])` to swap :stash in Status.
* BREAK: `(branches [this][this new-children])` for Node.
* BREAK: Decorator `:children` changed to `:child`.
* BREAK: Decorator `branch-cond-fn` gets Decorator whole instead of properties.
* BREAK: Decorator children print order changed due to `branches` use.

### Fixed

* Hooks handling and tests fixed (:on-\* to STATUS + :\*)
* Parent :hooks removed from Robin summary-action.

### Improved

* QOL: `wikla.core/Node` implemented for Vars (delegates via derefs).
* QOL: `wikla.dot/Dot` implemented for Vars (delegates via derefs).
* QOL: `wikla.zip/Zip` implemented for Vars (delegates via derefs).
* QOL: Default implementations of `wikla.dot/Dot` on Object/object.
* `wikla.core/detach-hook` to complement `wikla.core/attach-hook`.

## [0.7.0] - 2020-03-01

### Improved

* New `wikla.pre/bool-to-status` node

### Changed

* BREAK: `tick-fn` gets fed whole node, must return whole node.
* BREAK: Calling `(node)` is now `(tick! node)`, not `(tick node nil)`.
* BREAK: Eliminated Fragment due to `tick` rework.
* BREAK: `tick-fn` arity `[node stash]` and `[node]`, to match `tick`/`tick!`.
* BREAK: `success`, `failure`, `running` & `ready` arity match `tick`/`tick!`.
* BREAK: `return-result` is less "smart" now, but faster.
* BREAK: `run-hooks!` no longer swallows ANY exceptions. Callers beware. ;-)
* BREAK: Moved `to-success`, `to-failure` from core to pre.
* BREAK: Hooks moved from `:properties` to `[:properties :hooks]`.
* BREAK: Hooks on status keywords and `:*` for result hook.
* BREAK: Removed `wikla.act` and `clojure/core.async` dependency.
* QOL: Procedural, stash-less: `(tick! node)`->`(tick-fn node)`.
* QOL: Single arity `(all-tick node)` for stash-less execution.
* QOL: Stepper protocol for step-by-step implementation of tick.
* QOL: Stepper implementation for stash (Tick1) and stash-less (Tick0) ticks.
* QOL: `array-map` node properties template: `default-properties`.
* QOL: Properties wrapper utilizing `default-properties`: `wrap-properties`.
* QOL: Flag get/set to Node protocol: `(shift node)`+`(shift node new-flag)`.
* QOL: Label get/set to Node protocol: `(label node)`+`(label node new-name)`.
* QOL: Stepper tool: `(wikla.kit/step-until a-stepper pred)`
* QOL: Style guide on how to avoid performance penalties (like destructuring).
* QOL: `wikla.core/return-node` renamed to `resetable`, shim added.
* QOL: `wikla.core/resetable?` to see if initial node is available.
* QOL: `wikla.core/attach-hook` convenience function.
* Misc: Removed `plumatic.schema` dependency, ported to `clojure.spec.alpha`.
* Optimization: Destructuring over Record types replaced with direct access.
* Optimization: Direct calls to Result ctor as much as possible.
* Optimization: Status checks optimized around keyword comparisons.
* Optimization: Faster code path in join of core branches.
* Optimization: Result creation with raw constructor and reuse where sane.
* Optimization: Single arity `all-tick`, no stash, just side effects.
* Optimization: Constant things marked with `:const` meta.

### Fixed

* Fix: `wikla.kit/rewind` ignored any state other than the flag. Oopsie!

## [0.6.11] - 2018-08-26

### Improved

* New `wikla.core/node?` function
* New `wikla.pre/d:ready-on-running` decorator
* New `wikla.pre/a:exception` action
* New `wikla.zip/zipped-node` coercion
* New `wikla.kit` namespace

### Fixed

* Bug where Action and Condition would lose `:properties`, oopsie!

## [0.6.10] - 2018-06-03

### Improved

* `:on-ready` hook.
* `Context` node in `ext`, extracts part of stash for its children.
* `push-key` for defaut assoc push action for Context on literal key.

### Changed

* `Action` can now return Ready (along Success, Failure and Running).
* `Condition` can now return Ready (along Success and Failure).
* `Condition` now persists properties.
* `Decorator` properties from `own-tick` overwrite main `Decorator` properties.

## [0.6.9] - 2018-04-02

### Improved

* Added `wikla.core/run-hooks!` and node support.
* Dependency on `core.async` for some basic processing.
* Basic actor action in `wikla.act`.

### Changed

* `Transaction` can now be `tick`ed twice to prime and process.
* Codox cleanups (removed builtin constructors).

## [0.6.8]  - 2017-12-31

### Changed

* Extracted `Ticker`s `tick` into core function: `all-tick`.
* With `all-tick` basic branches are now `tick`able.
* `Ticker` is now a well behaving branch (`branching?`/`part`/`join`).
* Version bumps and Clojure 1.9.0 compatibility.

## [0.6.7] - 2017-08-26

### Fixed

* Printing of `wikla.ext/Until` and `wikla.ext/Unless`.

## [0.6.6] - 2017-08-22

### Improved

* Added `pre/d:map-result`, updated result mapping decorator constructors.
* Added `pre/d:failure-to-running` and `pre/d:success-to-running`.

## [0.6.5] - 2017-08-06

### Changed

* Added 0-arity `invoke`: `(node)` calls `(tick node nil)`.
* Improved examples a bit, touched up README.

### Fixed

* Added `applyTo` overload to `IFn` section of leaf nodes.

## [0.6.4] - 2017-07-30

### Changed

* `Until` and `Unless` nodes added to `wikla.ext`.
* Documentation touchups.

## [0.6.3] - 2017-03-18

### Improved

* Added a breakpoint: `wikla.pre/a:breakpoint`.

## [0.6.2] - 2017-03-16

### Improved

* Call leaves directly with stash to tick: `(tick T {})` => `(T {})`

## [0.6.1] - 2017-03-05

### Improved

* Added `to-success` and `to-failure`.
* Streamlined README and created CHANGELOG.

## [0.6.0] - 2016-12-03

### Improved

* Introduced `wikla.zip`.

### Changed

* API breakage: due to naming collisions between protocols, that cannot
  be mitigated with simple namespace segregation, methods in `Dot` protocol
  were renamed. Prefix `dot-` needs to be added to all implementations.

## [0.5.1] - 2016-11-19

### Improved

* Added `Transaction` node.

### Changed

* Set `:always-validate` on all constructors.

## [0.5.0] - 2016-03-31

### Improved

* All data-structures are now using Schema. Validation enabled in tests.

### Changed

* API breakage! Call convention changes!
* Additional properties of a node are now stored in `:properties`,
  not in the node itself:
  * This affects `Condition`s and `Action`s first and foremost.
  * This also caused `Decorator` to be more aligned to the paper and unable
      to actually modify its child in any way. Thus `d:shuffle` is gone.
  * `Decorator` actually uses `Action`s for `own-tick-action` and
      `post-join-action` now, therefore these functions are now bound
      to return Success/Failure/Running...
      and so `d:immediate` is gone as well, oops!
  * Tick functions are only fed properties now, not a `Node`. We don't
      really care about returned properties usually, so they can then either
      get returned on the tail call via `success`/`failure`/`running`
      or skipped (since they will be discarded anyway):

```clojure
(fn tick-fn [properties stash]
  (success properties stash))

;; is equivalent to

(fn tick-fn [properties stash]
  (success stash))
```

* New `Status` is introduced: `Fragment`. This is a `Result` analog that does
  not carry a `Node` (carries some data, stash and flag). Your tick functions
  will now be returning `Fragment`s, those will get repackaged into `Result`s
  by their callers. You should never get `Fragment` from `Node`/`tick`.
* Usage of `var`s encouraged for callbacks for readability. All examples will
  now be using this convention.
* No more `assert`ions, went full `ex-info`.
* Fixed tests, move from Midje convention `actual => expected`
  to `clojure.test` preferred `(is (= expected actual))` forms.
  FIXED ALL THEM TESTS!
* Split `wikla.ext` from `wikla.core` and `wikla.dot`, using it as example
  on how to extend the library:
  * Some weird, new node (`Robin`), with custom constructor
  * Implementing both `Node` and `Dot` protocols in `defrecord`
  * Tests for the above in own test namespace
* Also changed what `Robin` is and does, again. It now just re-runs Running
  children until all are Success or Failure, then it terminates with
  final call to `summary-fn` (wrapped in an `Action`). If that returns
  Running it `reset`s and goes back with Running. It's like `Parallel`,
  but with additional `Action` looking at stash, instead of preset thresholds
  condition being checked by the node directly.
* Refactored `wikla.dot`: updated some code, added flags to the generated
  graph and tweaked colors a bit (red is now orangered, instead of crimson).

## [0.4.1] - 2016-03-13

### Changed

* Modified `Robin` behavior.
* Documentation updated for Codox.

## [0.4.0] - 2016-01-24

### Improved

* ClojureScript support, courtesy of [Arron Mabrey][arronm].

## [0.3.2] - 2015-10-20

### Fixed

* Fixed exception handling in `Ticker` with `ex-info`.

## [0.3.1] - 2015-08-15

### Improved

* Added `wikla.dot` for tree visualization via dot from Graphviz.

## [0.3.0] - 2015-07-18

### Changed

* Re-done `Status`es.
* Added `Robin` node.
* Improved tests.
* Moved stuff.

## [0.2.0] - 2015-03-07

### Changed

* Merged `Ticker` with nodes.
* Moved predefined nodes to `pre`cooked.

## [0.1.2] - 2015-03-05

### Changed

* Simplified `Ticker`.
* Added marginalia.

## [0.1.1] - 2015-03-01

### Fixed

* Fixed in `Ticker`.
* Updated README.

## [0.1.0] - 2015-03-01

### Summary

Initial prototype.

[Unreleased]: https://bitbucket.org/spottr/wikla/branches/compare/master%0D1.0.3#diff
[1.0.3]: https://bitbucket.org/spottr/wikla/branches/compare/1.0.3%0D1.0.2#diff
[1.0.2]: https://bitbucket.org/spottr/wikla/branches/compare/1.0.2%0D1.0.1#diff
[1.0.1]: https://bitbucket.org/spottr/wikla/branches/compare/1.0.1%0D1.0.0#diff
[1.0.0]: https://bitbucket.org/spottr/wikla/branches/compare/1.0.0%0D0.9.5#diff
[0.9.5]: https://bitbucket.org/spottr/wikla/branches/compare/0.9.5%0D0.9.4#diff
[0.9.4]: https://bitbucket.org/spottr/wikla/branches/compare/0.9.4%0D0.9.3#diff
[0.9.3]: https://bitbucket.org/spottr/wikla/branches/compare/0.9.3%0D0.9.2#diff
[0.9.2]: https://bitbucket.org/spottr/wikla/branches/compare/0.9.2%0D0.9.1#diff
[0.9.1]: https://bitbucket.org/spottr/wikla/branches/compare/0.9.1%0D0.9.0#diff
[0.9.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.9.0%0D0.8.3#diff
[0.8.3]: https://bitbucket.org/spottr/wikla/branches/compare/0.8.3%0D0.8.2#diff
[0.8.2]: https://bitbucket.org/spottr/wikla/branches/compare/0.8.2%0D0.8.1#diff
[0.8.1]: https://bitbucket.org/spottr/wikla/branches/compare/0.8.1%0D0.8.0#diff
[0.8.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.8.0%0D0.7.0#diff
[0.7.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.7.0%0D0.6.11#diff
[0.6.11]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.11%0D0.6.10#diff
[0.6.10]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.10%0D0.6.9#diff
[0.6.9]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.9%0D0.6.8#diff
[0.6.8]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.8%0D0.6.7#diff
[0.6.7]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.7%0D0.6.6#diff
[0.6.6]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.6%0D0.6.5#diff
[0.6.5]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.5%0D0.6.4#diff
[0.6.4]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.4%0D0.6.3#diff
[0.6.3]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.3%0D0.6.2#diff
[0.6.2]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.2%0D0.6.1#diff
[0.6.1]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.1%0D0.6.0#diff
[0.6.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.6.0%0D0.5.1#diff
[0.5.1]: https://bitbucket.org/spottr/wikla/branches/compare/0.5.1%0D0.5.0#diff
[0.5.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.5.0%0D0.4.1#diff
[0.4.1]: https://bitbucket.org/spottr/wikla/branches/compare/0.4.1%0D0.4.0#diff
[0.4.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.4.0%0D0.3.2#diff
[0.3.2]: https://bitbucket.org/spottr/wikla/branches/compare/0.3.2%0D0.3.1#diff
[0.3.1]: https://bitbucket.org/spottr/wikla/branches/compare/0.3.1%0D0.3.0#diff
[0.3.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.3.0%0D0.2.0#diff
[0.2.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.2.0%0D0.1.2#diff
[0.1.2]: https://bitbucket.org/spottr/wikla/branches/compare/0.1.2%0D0.1.1#diff
[0.1.1]: https://bitbucket.org/spottr/wikla/branches/compare/0.1.1%0D0.1.0#diff
[0.1.0]: https://bitbucket.org/spottr/wikla/branches/compare/0.1.0%0D2822897e5cae63f91b221e0fa647a9e0b6da5de1#diff
[arronm]: https://bitbucket.org/arronmabrey/
