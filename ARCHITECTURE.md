# Architecture

`wikla` is a data oriented, deterministic, functional (immutability & purity)
implementation of Behavior Tree algorithm, in the AI, robotics and control
take.

Behavior Tree is a plan representation and execution tool. It forms a tree
graph, with leaves as actions and branches as flow control. Act of executing
a Behavior Tree once is called a tick.

Inspired by sections I-IV of [Towards a Unified Behavior Trees Framework for
Robot Control by Marzinotto et al. (2014)][2013-ICRA-mcko]
([mirror][2013-ICRA-mcko-mirror]) wikla tries to follow outlined conventions
closely as possible, while remaining Clojure-ish.

## Implementation

Nested immutable data structures implementing state transitions via protocols
over records. Idea here is that executed behavior tree is its own audit log,
as every node stores its final stash (blackboard) value.

All this is pure by default, using only immutable data. Blackboard, the shared
node data is implemented via stash field of nodes, handed down during `part`
and picked up in `join`. Getter-setter for it is called `yield`.

Flow control is done with SUCCESS, FAILURE and RUNNING flags, which should
perhaps be seen as "good, continue", "bad, abort" and "in progress, ask again."
These tree statuses will be reported outside of tick loop, SUCCESS and FAILURE
to signify tick completion, RUNNING to allow parking and re-entry.

For completeness a fourth flag is introduced: READY. It's the default status
for all freshly created nodes that were not yet ticked and can be used to ask
for immediate re-execution of a node. Unlike SUCCESS, FAILURE or RUNNING
it is not expected to be reported outside of the tick loop. The source paper
calls its analog NotTicked and only uses it for initial state.

Ticking is based on READY state, so node should be in that state before being
ticked or processed otherwise. RUNNING nodes will be implicitly shifted
to READY before being revisited and default `Stepper` implementation
on `Root` takes care of that. This also means that SUCCESS and FAILURE nodes
may and will short circuit their processing: when `tick`ed no action will take
plance, no children will be given by `part`, no joining children accepted
in `join`.

Allowed transitions are (RUNNING via implicit READY):

* SUCCESS => SUCCESS (terminated, no action)
* FAILURE => FAILURE (terminated, no action)
* RUNNING => SUCCESS | FAILURE | RUNNING | READY
* READY => SUCCESS | FAILURE | RUNNING | READY

### Protocols

There are two key protocols: one for tree nodes, other the tick algorithm.

#### Node

[Node protocol][node-protocol] defines the behavior tree node operations.

Special method `-node? => Boolean` is a taxonomy check, defined on every node,
object base class and on `nil`. It's used by the `(node? [o]) => Boolean`
predicate, utilized by specs.

##### Getters and Setters

Protocol defines get-set operations for common node contents. The same method
name is used for both get and set, method signature is:

```clojure
(method node) => data

(method node new-data) => node w/ new-data
```

Flag get-set is done via `shift` method, this is used by the status checking
and setting API (`success?`/`success` and friends). Node name can be get-set
with `label` shortcut method. Hooks can be set via `hooks` call, which gives
and accepts immutable map -- there is a higher level API for that too. Node
properties can be get-set via `props` method and parameters via `params`
invocation. They return and accept maps. All children nodes of a branch
node are made available via `branches` and this method is vital point for
implementations of both node zipping and streaming. `stem` getter is nodes
identity function, while setter swaps the stash and returns new node with
it. Finally `yield` is the method that gets and sets the stash (shared data
aka blackboard).

##### Ticking Related Operations

Predicate method `branch?` is used to determine a way of handling node,
determines if it will be `part`ed (on `true`) or `tick`ed next (on `false`).

```clojure
(branch? node) => Boolean
```

Composite nodes provide children for execution via part:

```clojure
(part composite-node) => child-node
```

And accept returning children via join:

```clojure
(join composite-node child-node') => composite-node'
```

Leaf nodes get ticked, perform an action and return updated node:

```clojure
(tick leaf-node) => leaf-node'
```

Nodes can be provide both branching and ticking functionality, see
`Decorator`.

##### DOTs

Several visualization related methods are defined for each and every node
to help with DOT generation. `dot-shape` returns a shape of the node
corresponding with [graphviz][graphviz]'s available shapes. `dot-label` produces
a string which is either a class of the node or a class combined with
node's actual label. `dot-color` maps current flag to corresponding color.

#### Stepper

[Stepper protocol][stepper-protocol] defines the behavior tick algorithm.

Special method `-stepper? => Boolean` is a taxonomy check, defined on `Root`,
object base class and on `nil`. It's used by the `(stepper? [o]) => Boolean`
predicate, utilized by specs.

Protocol delivers two ways of executing the tick: step by step and complete.

Step by step execution can be achieved via `(step root) => root'` for single
logical operation. `(step-until root pred) => root'` allows to jump over
several ticks until `(pred root)` is true. Protocol provides a tick completion
predicate `(finished? root)` which can be used with `step-until`. The default
stepping implementation on `Root` delegates tree handling to two sub-methods:
`(handle-branch root) => root'` and `(handle-leaf root) => root'`.

Complete tick is provided by `(exhaust root) => root'`. It does not delegate
any tasks to other methods in default implementation on `Root`.

### Nodes

#### Action

[Action][action-doc] is a leaf node wrapping a function.

When `(tick node)` is executed on Action it first checks current flag.
If it's already SUCCESS or FAILURE then `tick-fn` will not be executed, instead
action node will be immediately returned. If flag is set to RUNNING or READY
then result of `(tick-fn node)` is returned as expected.

The idea is that `tick-fn` interacts with the node via `shift`, `params`,
`yield` and friends and returns node `shift`ed to something other than
READY. Returning with READY is how you ask to get called again.

API helpers can be used affect `shift` and `yield` in one call:

```clojure
(require '[wikla.tree :refer [params yield success]])

(def tick-fn [self]
  (let [{:keys [lo hi] :or {lo 0.0 hi 1.0}} (params self)
        v (yield self)]
    (if (<= lo v hi)
      (success self)
      (success self (max (min v hi) lo)))))
```

A shortcut `(fn->action tick-fn)` is provided next to the `(action seed)`
constructor.

#### Condition

[Condition][condition-doc] is a node wrapping a predicate.

When `(tick node)` is executed on Condition it first checks current flag.
If it's SUCCESS or FAILURE then `pred-fn` will not be executed, instead
Condition node is immediately returned. Otherwise `(pred-fn note)` runs
and truth-y result sets it to SUCCESS, non-truth-y to FAILURE.

```clojure
(require '[wikla.tree :refer [params yield success]])

(def pred-fn [self]
  (let [{:keys [lo hi] :or {lo 0.0 hi 1.0}} (params self)
        v (yield self)]
    (<= lo v hi)))
```

A shortcut `(fn->condition pred-fn)` is provided next to the
`(condition seed)` constructor.

#### Selector

[Selector][selector-doc] is a composite node that succeeds if any child
succeeded and fails if all children failed. Implements most of `Node`
protocol, with exception of `tick` which is not implemented and `params`
implemented as no-op.

Selector is a node that executes children in order and wants at most a single
SUCCESS. It will only fail if all children signal FAILURE. RUNNING child
causes Selector to park with RUNNING too.

Calling `(part node)` returns next child to be executed, with stash transferred
from Selector to child. When this child is decided call `(join node child)`
causes Selector to decide its own flag and grab the stash from returning child.

Selector transitions on child's flag:

* SUCCESS => SUCCESS
* FAILURE => READY with next child or FAILURE if no children left
* RUNNING => RUNNING

#### Sequence

[Sequence][sequence-doc] is a composite node that fails if any child failed
and succeeds if all children succeeded. Implements most of `Node`
protocol, with exception of `tick` which is not implemented and `params`
implemented as no-op.

Sequence is a node that executes children in order and wants all children
to flag SUCCESS. It will fail on first child FAILURE. RUNNING child causes
Sequence to park with RUNNING too.

Calling `(part node)` returns next child to be executed, with stash transferred
from Sequence to child. When this child is decided call `(join node child)`
causes Sequence to decide its own flag and grab the stash from returning child.

Selector transitions on child's flag:

* SUCCESS => READY with next child or SUCCESS if no children left
* FAILURE => FAILURE
* RUNNING => RUNNING

#### Parallel

[Parallel][parallel-doc] is a node that runs all children "in parallel" until
none of them is RUNNING anymore. Then it tallies success and failure among
them to see what the final result is. Retries if neither threshold is reached.

Child returning RUNNING gets parked and Parallel signals READY with next child.
Children returning SUCCESS or FAILURE are excluded from next cycle of execution.
Once a cycle is completed Parallel will signal `:cycle-flag`, READY by default.

Final SUCCESS v. FAILURE count happens once all children are done. If success
threshold is reached or crossed then SUCCESS is returned. If that is not the
case and failure threshold is reached or crossed then FAILURE is returned.
If none of these are true then a retry is attempted. By default a retry
counter is increased and a fresh copy of children is substituted before
Parallel signals `:retry-flag`, which is READY by default.

Alternative strategy `(rewind-fn node)` can be seeded with `:rewind-fn` key.
It will get fed Parallel with `:retry-flag` set and `:retry` counter
incremented.

Parallel transitions on child's flag:

* SUCCESS => cycle-flag if end of cycle; retry-flag if end of run without
  threshold reached; READY with next child otherwise
* FAILURE => cycle-flag if end of cycle; retry-flag if end of run without
  threshold reached; READY with next child otherwise.

For alternative implementation see [Robin][robin-doc].

#### Decorator

[Decorator][decorator-doc] is a node that has one child and it might either
provide it for execution, and then decide what to return when that is joined,
or tick itself.

If Decorator is SUCCESS or FAILURE no further execution happens.

It presents itself as leaf node first, during the tick if child is not READY
the Decorator will attempt to execute `(post-join-fn decorator) => decorator'`.
If child is READY, then branching predicate `(branch-pred decorator) => Boolean`
is executed and based on that the Decorator will either change into a branch,
presenting child for execution or execute its own tick.

`branch-pred` returned true: Decorator switches to be a branch and signals
READY. Calls to `part` and `join` are expected. During the latter Decorator
will check for if `post-join-fn` is set. If it is not present it just grabs
flag and stash from the child and propagates those. If it is present a call
`(post-join-fn decorator') => decorator''` will be executed and its result
returned instead.

`branch-pred` returned false: `(own-tick-fn decorator) => decorator'` runs
instead.

#### Root

[Root][root-doc] is the node that runs the tick loop. It has a single child,
which is a tree it will execute on tick. Several method calls of Root will
get proxied directly to/from the tree: `shift`, `params`, `yield` and `hooks`.

It is the reference implementation of `Stepper` protocol.

It does not allow to be treated as branch, implements `tick` instead, which
delegates work to `exhaust` from `Stepper`.

#### Robin

[Robin][robin-doc] is a [Parallel][parallel-doc] that does not check thresholds,
but rather calls a `(summary-fn robin) => robin'` from `:summary-fn` to determine
the final outcome when all children are done. Other than that it's pretty much
identical, including `:rewind-fn` functionality, `:retry-flag` and `:cycle-flag`
customization.

Child returning RUNNING gets parked and Robin signals READY with next child.
Children returning SUCCESS or FAILURE are excluded from next cycle of execution.
Once a cycle is completed Robin will signal `:cycle-flag`, READY by default.

Once all children are done the `(summary-fn robin) => robin'` call will take
place. If node it returns is SUCCESS or FAILURE then that is that. Otherwise
(RUNNING or READY) a rewind happens, `:retry` counter gets incremented and
node returns with `:retry-flag`, READY by default.

Alternative strategy `(rewind-fn node)` can be seeded with `:rewind-fn` key.
It will get fed Robin with `:retry-flag` set and `:retry` counter incremented.

Robin transitions on child's flag:

* SUCCESS => cycle-flag if end of cycle; retry-flag if end of run without
  conclusive summary; READY with next child otherwise
* FAILURE => cycle-flag if end of cycle; retry-flag if end of run without
  conclusive summary; READY with next child otherwise

For more canonical implementation see [Parallel][parallel-doc].

## API

Functions defined in `wikla.tree` for ease of use. There are several predicates
and combined setters here for combining common usage situations.

### Predicates and Status Helpers

Higher level taxonomy checks are defined as top level functions `node?`
and `stepper?`, these accept arbitrary object and return a boolean.

The `success?`, `failure?`, `running?`, `ready?`, `done?` and `decided?`
predicates allow keyword optimized checks for `shift` of a node. `done?`
is a combination of (SUCCESS or FAILURE) check, `decided?` is (SUCCESS
or FAILURE or RUNNING).

The `success`, `failure`, `running` and `ready` setters come with in two
flavors:

```clojure

(success node) ;; (shift node SUCCESS)

(success node stash) ;; (yield (shift node SUCCESS) stash)
```

### Action Ops

Several `update` like helper functions have been defined to ease `action-fn`
writing that are thread-first (`->`) macro friendly when threading `node`:

* `in-stash` modifies stash/blackboard of current node
* `in-props` operates on properties of current node
* `in-params` touches parameters of current node
* `merge-props` operates on properties of given sub-tree
* `merge-params` operates on parameters of given sub-tree

### Hooks

Low level interface for hooks is implemented via `hooks` get-set method,
which gives and expects a map of status keyword (or `:*` for catch-all)
to callable. During `wikla.tree/run-hooks!` hooks are selected based
on `shift` of a node, then fed the node it's running for, its return value
discarded. The catch-all hook bound to `:*` runs always if present, even when
more direct hook was present.

**Hooks only run during `Stepper` driven `Root` tick.**

User interface to hooks is:

```clojure
(attach-hook node SUCCESS a-fn) ;; => node' w/ a-fn as SUCCESS hook

(attach-hook node a-fn) ;; => node' w/ a-fn as :* hook

(detach-hook node SUCCESS) ;; => node w/ SUCCESS hook removed

(detach-hook node) ;; => node w/ :* hook removed
```

### Useful Things

Some helpers were introduced to make life a bit easier. Nodes are a tree,
so a natural candidate for `clojure.zip` interface. The `wikla.tree/zipper`
functionality is solely based on `branches` implementation in `Node`.

Thanks to `branches` it was also trivial to implement `wikla.tree/node-seq`
to stream nodes based on `tree-seq`. Tree analysis with built-in `seq`
functions is a thing.

Simple REPL-targeted helpers to print tree and stack to STDOUT can be found
at `wikla.tree/print-tree` and `wikla.tree/print-callstack`.

Tiny interns: `wikla.tree/yes`, `wikla.tree/no` and `wikla.tree/mu` are there
for when you need a dumb predicate to plug.

## Specifications

Specifications are separate from main implementation defined in `wikla.specs`
namespace. This namespace is dependent on `wikla.tree`, as it imports
definitions from there on which it builds the specs. The spec are then
attached to the functions defined in the main namespace.

[2013-ICRA-mcko]: http://www.csc.kth.se/~miccol/Michele_Colledanchise/Publications_files/2013_ICRA_mcko.pdf
[2013-ICRA-mcko-mirror]: https://spottr.bitbucket.io/wikla/2013_ICRA_mcko.pdf
[graphviz]: http://www.graphviz.org/
[node-protocol]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-Node
[stepper-protocol]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-Stepper
[action-doc]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-action
[condition-doc]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-condition
[selector-doc]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-selector
[sequence-doc]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-sequence
[parallel-doc]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-parallel
[decorator-doc]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-decorator
[root-doc]: https://spottr.bitbucket.io/wikla/latest/wikla.tree.html#var-root
[robin-doc]: https://spottr.bitbucket.io/wikla/latest/wikla.ext.html#var-robin
