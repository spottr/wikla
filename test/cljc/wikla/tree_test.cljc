;; Copyright (c) Sławek Gwizdowski
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this poftware and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, publicense,
;; and/or pell copies of the Software, and to permit persons to whom the
;; Software is furnished to do po, pubject to the following conditions:

;; The above copyright notice and this permission notice phall be included
;; in all copies or pubstantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
;; IN THE SOFTWARE.
;;
(ns wikla.tree-test
  "Tests for tree."
  (:refer-clojure :exclude [sequence])
  (:require
   #?(:cljs [cljs.test    :as t :refer-macros [deftest testing is]]
      :clj  [clojure.test :as t :refer        [deftest testing is]])
   #?(:clj  [orchestra.spec.test :as st]
      :cljs [orchestra-cljs.spec.test :as st])
   [wikla.tree :as wikla
    :refer [;; Node
            -node? shift label hooks props params branches stem yield branch?
            part  join tick dot-shape dot-label dot-color
            ;; Stepper
            -stepper? finished? step step-until
            callstack exhaust
            node? stepper?
            ;; flags
            SUCCESS FAILURE RUNNING READY dot-colors
            success? success failure? failure running? running ready? ready
            done? decided?
            ;; hooks
            attach-hook detach-hook run-hooks!
            ;; Other
            zipper print-tree print-callstack dtx
            node-seq yes no mu
            in-stash in-props in-params merge-props merge-params friendlify
            ;; Nodes
            action fn->action
            condition fn->condition
            selector as-selector selector*
            sequence as-sequence sequence*
            parallel as-parallel parallel*
            decorator decorate
            root root-of
            robin as-robin robin*
            ;; Predefs
            success-action failure-action running-action
            success-condition failure-condition
            ;; Helpers
            to-success to-failure via-pred]]
   [wikla.specs]
   [clojure.zip]))

(st/instrument)
(st/unstrument ['yes 'ready])

;; Helper definitions

(defn default-hooks
  "Given a volatile creates status counter hooks updating it."
  [state]
  (let [just-inc (fnil inc 0)]
    {SUCCESS (fn [_] (vswap! state update :success just-inc))
     FAILURE (fn [_] (vswap! state update :failure just-inc))
     RUNNING (fn [_] (vswap! state update :running just-inc))
     READY   (fn [_] (vswap! state update :ready just-inc))
     :*      (fn [_] (vswap! state update :result just-inc))}))

(defn tick-trace
  "Runs given status callable on node with kw added to stash."
  [status kw]
  #(status %1 (conj (or (yield %1) []) kw)))

(defn part-tick-join1
  "Part child, tick it, join it back."
  [node]
  (join node (tick (part node))))

(defn part-tick-join
  "Given a branch one level runs through children."
  [node]
  (if (and (not (decided? node)) (branch? node))
    (recur (part-tick-join1 node))
    node))

(defn can-call?
  "Try to run method m on node n with arguments args. Returns false
  on AbstractMethodException, true otherwise.

  Method better be free of side effects, because it is being called."
  [n m & args]
  (try (apply m n args) true
       (catch #?(:clj AbstractMethodError :cljs js/Error) _ false)))

;; Test bundles for phared APIs

(defn test-shared-getters-setters
  "Common getters petters test."
  [node]
  (is (-node? node))
  (is (node? node))
  (is (= READY   (shift (shift node READY))))
  (is (= SUCCESS (shift (shift node SUCCESS))))
  (is (= FAILURE (shift (shift node FAILURE))))
  (is (= RUNNING (shift (shift node RUNNING))))
  (is (= "x" (label (label node "x"))))
  (is (or (and (= node (params node {:x true}))
               (nil? (params (params node {:x true})))) ;; nop-params
          (= {:x true} (params (params node {:x true})))))
  (is (= {:* identity} (hooks (hooks node {:* identity}))))
  (is (= {:x true} (props (props node {:x true}))))
  (if (seq (branches node))
    (is (= [(action {:tick-fn success})]
           (branches (branches node [(action {:tick-fn success})]))))
    (is (nil? (branches (branches node [(action {:tick-fn success})])))))
  (is (= node (stem node)))
  (is (= (shift (yield node 1) RUNNING)
         (stem (yield node 1) (shift node RUNNING))))
  (is (= 1 (yield (yield node 1))))
  (is (boolean? (branch? node)))
  (is (string? (dot-shape node)))
  (is (string? (dot-label node)))
  (is (string? (dot-color node)))
  (is (= (get dot-colors (shift node)) (dot-color node)))
  (is (= (get dot-colors SUCCESS) (dot-color (shift node SUCCESS))))
  (is (= (get dot-colors FAILURE) (dot-color (shift node FAILURE))))
  (is (= (get dot-colors RUNNING) (dot-color (shift node RUNNING)))))

(defn test-shared-api-calls
  "Common API calls test."
  [node]
  (is (success? (success node)))
  (is (= [true :ok] ((juxt success? yield) (success node :ok))))
  (is (failure? (failure node)))
  (is (= [true :ops] ((juxt failure? yield) (failure node :ops))))
  (is (running? (running node)))
  (is (= [true :wait] ((juxt running? yield) (running node :wait))))
  (is (ready? (ready node)))
  (is (= [true :k!] ((juxt ready? yield) (ready node :k!))))
  (is (and (done? (success node))
           (done? (failure node))))
  (is (and (decided? (success node))
           (decided? (failure node))
           (decided? (running node))))
  (is (= {SUCCESS identity} (hooks (attach-hook node SUCCESS identity))))
  (is (= {:* identity} (hooks (attach-hook node identity))))
  (is (= {:* vec SUCCESS inc FAILURE dec RUNNING identity}
         (-> (attach-hook node vec)
             (attach-hook FAILURE dec)
             (attach-hook SUCCESS inc)
             (attach-hook RUNNING identity)
             hooks)))
  (is (= {SUCCESS inc RUNNING vec :* identity}
         (hooks (detach-hook (hooks node {FAILURE dec
                                          SUCCESS inc
                                          RUNNING vec
                                          :*      identity})
                             FAILURE))))
  (is (= {FAILURE dec}
         (hooks (detach-hook (hooks node {:* identity FAILURE dec})))))
  (is (= (cons node (branches node)) (node-seq node)))
  (is (= node (clojure.zip/node (zipper node))))
  (is (= (meta node)
         (-> (zipper node)
             (clojure.zip/edit identity)
             (clojure.zip/node)
             meta)))
  (let [state (volatile! nil)]
    (run-hooks! (success (hooks node (default-hooks state))))
    (is (= {:success 1 :result 1} @state)))
  (let [state (volatile! nil)]
    (run-hooks! (failure (hooks node (default-hooks state))))
    (is (= {:failure 1 :result 1} @state)))
  (let [state (volatile! nil)]
    (run-hooks! (running (hooks node (default-hooks state))))
    (is (= {:running 1 :result 1} @state)))
  (let [state (volatile! nil)]
    (run-hooks! (ready (hooks node (default-hooks state))))
    (is (= {:ready 1 :result 1} @state))))

;; Useful Things tests

(deftest Printing-node-tree
  (testing "Testing printing node tree with and without mark"
    (let [s (sequence
             {:children [(sequence
                          {:children [(condition {:pred-fn yes})
                                      (condition {:pred-fn no})]})]})]
      (is (= (str " Sequence [Ready]" \newline
                  "   Sequence [Ready]" \newline
                  "     Condition [Ready]" \newline
                  "     Condition [Ready]" \newline)
             (try (with-out-str (print-tree s))
                  (catch #?(:clj Exception :cljs :default) e
                    (println e)))))
      (is (= (str " Sequence [Ready]" \newline
                  "   Sequence [Ready]" \newline
                  "*    Condition [Ready]" \newline
                  "*    Condition [Ready]" \newline)
             (with-out-str (print-tree :pred-fn s)))))))

(deftest Printing-node-callstack
  (testing "Testing callstack printing Ready and Running."
    (let [r (root-of (sequence* (action {:tick-fn running})))
          s (root-of (sequence* (action {:tick-fn success})))
          f (root-of (sequence* (action {:tick-fn failure})))]
      (is (= (str "Sequence [Ready]" \newline)
             (with-out-str (print-callstack r))))
      (is (= (str "Sequence [Running]" \newline)
             (with-out-str (print-callstack (tick r)))))
      (is (= (str "Sequence [Success]" \newline)
             (with-out-str (print-callstack (tick s)))))
      (is (= (str "Sequence [Failure]" \newline)
             (with-out-str (print-callstack (tick f))))))))

(deftest Stepping-down-to-exception
  (testing "Stepping down to exception with and without throw"
    (let [f (fn [self]
              (if (:throw (yield self))
                (throw (ex-info "I throw now." {:self self}))
                (success self)))
          t (root-of (sequence* (action {:tick-fn f})))
          s (dtx (in-stash t assoc :throw false))
          x (try (dtx (in-stash t assoc :throw true))
                 (catch #?(:clj Exception :cljs ExceptionInfo) ex
                   ((get (ex-data ex) :root))))]
      (is (node? (tick (yield t {:throw false}))))
      (is (node? (dtx (yield t {:throw false}))))
      (is (thrown? #?(:clj Exception :cljs ExceptionInfo)
                   (tick (yield t {:throw true}))))
      (is (thrown? #?(:clj Exception :cljs ExceptionInfo)
                   (dtx (yield t {:throw true}))))
      (is (= (str "Sequence [Success]" \newline)
             (with-out-str (print-callstack s))))
      (is (= (str "Sequence [Ready]" \newline
                  "  Action [Ready]" \newline)
             (with-out-str (print-callstack x)))))))

(deftest Node-streaming-aka-node-seq
  (testing "Protocol implementation of branches and leaves"
    (let [c (fn->condition yes)
          a (fn->action success)
          p (parallel* c a)
          o (robin* p c a)
          x (selector* o a c)
          s (sequence* x)
          r (root-of s)]
      (is (= [r s x o p c a c a a c] (vec (node-seq r)))))))

(deftest All-the-small-things
  (testing "Interned constants tests"
    (is (true? (yes)))
    (is (true? (yes :ok :then)))
    (is (false? (no)))
    (is (false? (no :not :now)))
    (is (nil? (mu)))
    (is (nil? (mu :what :if)))))

(deftest Stash-in-place-smash-helper
  (testing "In place stash update tests"
    (let [a (action {:tick-fn success :stash []})]
      (is (= [] (yield (in-stash a identity))))
      (is (= [1] (yield (in-stash a conj 1))))
      (is (= [1 2] (yield (in-stash a conj 1 2))))
      (is (= [1 2 3] (yield (in-stash a conj 1 2 3))))
      (is (= [1 2 3 4] (yield (in-stash a conj 1 2 3 4)))))))

(deftest Props-in-place-smash-helper
  (testing "In place params update tests"
    (let [a (action {:tick-fn success :properties {}})]
      (is (= {} (props (in-props a identity))))
      (is (= {:a 1} (props (in-props a assoc :a 1)))))))

(deftest Params-in-place-smash-helper
  (testing "In place params update tests"
    (let [a (action {:tick-fn success :parameters {}})]
      (is (= {} (params (in-params a identity))))
      (is (= {:a 1} (params (in-params a assoc :a 1)))))))

;; Shared API tests

(deftest Action-shared-api-tests
  (testing "Shared node getters-setters"
    (test-shared-getters-setters (action {:tick-fn success}))
    (test-shared-api-calls (action {:tick-fn success}))))

(deftest Condition-shared-api-tests
  (testing "Shared node getters-setters"
    (test-shared-getters-setters (condition {:pred-fn yes}))
    (test-shared-api-calls (condition {:pred-fn yes}))))

(deftest Selector-shared-api-tests
  (testing "Shared node getters-setters"
    (test-shared-getters-setters
     (selector {:children [(action {:tick-fn success})]}))
    (test-shared-api-calls
     (selector {:children [(action {:tick-fn success})]}))))

(deftest Sequence-shared-api-tests
  (testing "Shared node getters-setters"
    (test-shared-getters-setters
     (sequence {:children [(action {:tick-fn success})]}))
    (test-shared-api-calls
     (sequence {:children [(action {:tick-fn success})]}))))

(deftest Parallel-shared-api-tests
  (testing "Shared node getters-setters"
    (test-shared-getters-setters (parallel {:children [(action {:tick-fn success})]}))
    (test-shared-api-calls (parallel {:children [(action {:tick-fn success})]}))))

(deftest Decorator-shared-api-tests
  (testing "Shared node getters-setters"
    (test-shared-getters-setters
     (decorator {:child (action {:tick-fn success})}))
    (test-shared-api-calls
     (decorator {:child (action {:tick-fn success})}))))

(deftest Root-shared-api-tests
  (testing "Shared node getters-setters"
    (test-shared-getters-setters
     (root {:tree (action {:tick-fn success})}))
    (test-shared-api-calls
     (root {:tree (action {:tick-fn success})}))))

(deftest Robin-shared-api-tests
  (testing "Shared node getters-setters"
    (test-shared-getters-setters
     (robin {:children [(action {:tick-fn success})]}))
    (test-shared-api-calls
     (robin {:children [(action {:tick-fn success})]}))))

;; Action Tests

(deftest Action-construction-tests
  (testing "Action construction with full seed"
    (let [a (action {:tick-fn    running
                     :flag       SUCCESS
                     :properties {:x true}
                     :parameters {:x 0 :y 0}
                     :stash      :ok})]
      (is (success? a))
      (is (true? (get-in a [:properties :x])))
      (is (= {:x 0 :y 0} (params a)))
      (is (= :ok (yield a)))))
  (testing "Action construction with minimal seed"
    (let [a (action {:tick-fn ready})]
      (is (ready? a))
      (is (nil? (:properties a)))
      (is (nil? (params a)))
      (is (nil? (yield a)))
      (is (= a (action a)))
      (is (= (label a (friendlify ready)) (fn->action ready)))
      (is (= "A" (label (fn->action "A" ready))))
      (is (= (friendlify ready) (label (fn->action ready))))
      (is (= #?(:clj "wikla.tree/ready" :cljs "wikla.tree.ready")
             (label (fn->action ready)))))))

(deftest Action-tick-tests
  (testing "Action, ticking when not done"
    (let [node   (yield (fn->action #(success % :ticked)) :not-ticked)
          result (juxt shift yield)]
      (is (= [READY   :not-ticked] (result node)))
      (is (= [SUCCESS :ticked]     (result (tick node))))
      (is (= [SUCCESS :not-ticked] (result (tick (success node)))))
      (is (= [FAILURE :not-ticked] (result (tick (failure node)))))
      (is (= [SUCCESS :ticked]     (result (tick (running node))))))))

;; Condition Tests

(deftest Condition-construction-tests
  (testing "Condition construction with full seed"
    (let [c (condition {:pred-fn    yes
                        :flag       SUCCESS
                        :properties {:x true}
                        :parameters {:x 0
                                     :y 0}
                        :stash      :ok})]
      (is (success? c))
      (is (true? (get-in c [:properties :x])))
      (is (= {:x 0 :y 0} (params c)))
      (is (= :ok (yield c)))))
  (testing "Condition construction with minimal seed"
    (let [c (condition {:pred-fn yes})]
      (is (ready? c))
      (is (nil? (:properties c)))
      (is (nil? (params c)))
      (is (nil? (yield c)))
      (is (= c (condition c)))
      (is (= (label c (friendlify yes)) (fn->condition yes)))
      (is (= "C" (label (fn->condition "C" yes))))
      (is (= (friendlify yes) (label (fn->condition yes))))
      (is (= #?(:clj "wikla.tree/yes" :cljs "wikla.tree.yes")
             (label (fn->condition yes)))))))

(deftest Condition-tick-tests
  (testing "Condition, ticking when not done"
    (let [node   (fn->condition (comp (partial = :orig) yield))
          result (juxt shift yield)]
      (is (= [READY   nil]   (result node)))
      (is (= [SUCCESS :orig] (result (tick (yield node :orig)))))
      (is (= [FAILURE :diff] (result (tick (yield node :diff))))))))

;; Selector Tests

(deftest Selector-construction-tests
  (testing "Selector construction with full seed"
    (let [s (selector {:current    1
                       :flag       SUCCESS
                       :properties {:x true}
                       :stash      :ok
                       :children
                       [(action {:flag SUCCESS :tick-fn success})
                        (action {:tick-fn failure})
                        (action {:tick-fn running})]})]
      (is (success? s))
      (is (= 1 (:current s)))
      (is (true? (get-in s [:properties :x])))
      (is (= 3 (count (branches s))))
      (is (= :ok (yield s)))
      (is (= s (selector s)))))
  (testing "Selector construction with minimal seed"
    (let [s (selector {:children [(action {:tick-fn running})]})]
      (is (ready? s))
      (is (= 0 (:current s)))
      (is (nil? (:properties s)))
      (is (nil? (yield s)))
      (is (= 1 (count (branches s))))
      (is (= s (as-selector [(action {:tick-fn running})])))
      (is (= s (selector* (action {:tick-fn running})))))))

(deftest Selector-part-and-join-tests
  (testing "Selector handover: part"
    (let [a (action {:stash :original :tick-fn identity})
          p (selector {:stash :parental :children [a]})]
      (is (= (yield a :parental) (part p)))))
  (testing "Selector handover: join"
    (let [a (action {:stash :original :tick-fn running})
          s (selector {:stash :parental :children [a]})]
      (is (success? (join s (success (part s) :ticked))))
      (is (= :ticked (yield (join s (success (part s) :ticked)))))
      (is (failure? (join s (failure (part s) :ticked))))
      (is (= :ticked (yield (join s (failure (part s) :ticked)))))
      (is (running? (join s (running (part s) :ticked))))
      (is (= :ticked (yield (join s (running (part s) :ticked)))))))
  (testing "Selector multistage part and join:"
    (let [as (action {:tick-fn (tick-trace success :success)})
          af (action {:tick-fn (tick-trace failure :failure)})
          ar (action {:tick-fn (tick-trace running :running)})]
      (let [t (part-tick-join (selector {:children [as as as]}))]
        (is (= [:success] (yield t)))
        (is (success? t)))
      (let [t (part-tick-join (selector {:children [af as as]}))]
        (is (= [:failure :success] (yield t)))
        (is (success? t)))
      (let [t (part-tick-join (selector {:children [af af as]}))]
        (is (= [:failure :failure :success] (yield t)))
        (is (success? t)))
      (let [t (part-tick-join (selector {:children [af af af]}))]
        (is (= [:failure :failure :failure] (yield t)))
        (is (failure? t)))
      (let [t (part-tick-join (selector {:children [ar as as]}))]
        (is (= [:running] (yield t)))
        (is (running? t))
        (let [t* (part-tick-join (join t (success (part t))))]
          (is (= [:running] (yield t*)))
          (is (success? t*)))
        (let [t* (part-tick-join (join t (failure (part t))))]
          (is (= [:running :success] (yield t*)))
          (is (success? t*))))
      (let [t (part-tick-join (selector {:children [af ar as]}))]
        (is (= [:failure :running] (yield t)))
        (is (running? t))
        (let [t* (part-tick-join (join t (success (part t))))]
          (is (= [:failure :running] (yield t*)))
          (is (success? t*)))
        (let [t* (part-tick-join (join t (failure (part t))))]
          (is (= [:failure :running :success] (yield t*)))
          (is (success? t*))))
      (let [t (part-tick-join (selector {:children [af af ar]}))]
        (is (= [:failure :failure :running] (yield t)))
        (is (running? t))
        (let [t* (part-tick-join (join t (failure (part t))))]
          (is (= [:failure :failure :running] (yield t*)))
          (is (failure? t*)))
        (let [t* (part-tick-join (join t (success (part t))))]
          (is (= [:failure :failure :running] (yield t*)))
          (is (success? t*)))))))

;; Sequence Tests

(deftest Sequence-construction-tests
  (testing "Sequence construction with full seed"
    (let [s (sequence {:current    1
                       :flag       SUCCESS
                       :properties {:x true}
                       :stash      :ok
                       :children
                       [(action {:flag SUCCESS :tick-fn success})
                        (action {:tick-fn failure})
                        (action {:tick-fn running})]})]
      (is (success? s))
      (is (= 1 (:current s)))
      (is (true? (get-in s [:properties :x])))
      (is (= 3 (count (branches s))))
      (is (= :ok (yield s)))
      (is (= s (sequence s)))))
  (testing "Sequence construction with minimal seed"
    (let [s (sequence {:children [(action {:tick-fn running})]})]
      (is (ready? s))
      (is (= 0 (:current s)))
      (is (nil? (:properties s)))
      (is (nil? (yield s)))
      (is (= 1 (count (branches s))))
      (is (= s (as-sequence [(action {:tick-fn running})])))
      (is (= s (sequence* (action {:tick-fn running})))))))

(deftest Sequence-part-and-join-tests
  (testing "Sequence handover: part"
    (let [a (action {:stash :original :tick-fn identity})
          s (sequence {:stash :parental :children [a]})]
      (is (= (yield a :parental) (part s)))))
  (testing "Sequence handover: join"
    (let [a (action {:stash :original :tick-fn identity})
          s (sequence {:stash :parental :children [a]})]
      (is (success? (join s (success (part s) :ticked))))
      (is (= :ticked (yield (join s (success (part s) :ticked)))))
      (is (failure? (join s (failure (part s) :ticked))))
      (is (= :ticked (yield (join s (failure (part s) :ticked)))))
      (is (running? (join s (running (part s) :ticked))))
      (is (= :ticked (yield (join s (running (part s) :ticked)))))))
  (testing "Sequence multistage part and join:"
    (let [as (action {:tick-fn (tick-trace success :success)})
          af (action {:tick-fn (tick-trace failure :failure)})
          ar (action {:tick-fn (tick-trace running :running)})]
      (let [t (part-tick-join (sequence {:children [as as as]}))]
        (is (= [:success :success :success] (yield t)))
        (is (success? t)))
      (let [t (part-tick-join (sequence {:children [af as as]}))]
        (is (= [:failure] (yield t)))
        (is (failure? t)))
      (let [t (part-tick-join (sequence {:children [as af as]}))]
        (is (= [:success :failure] (yield t)))
        (is (failure? t)))
      (let [t (part-tick-join (sequence {:children [as as af]}))]
        (is (= [:success :success :failure] (yield t)))
        (is (failure? t)))
      (let [t (part-tick-join (sequence {:children [ar as as]}))]
        (is (= [:running] (yield t)))
        (is (running? t))
        (let [t* (part-tick-join (join t (success (part t))))]
          (is (= [:running :success :success] (yield t*)))
          (is (success? t*)))
        (let [t* (part-tick-join (join t (failure (part t))))]
          (is (= [:running] (yield t*)))
          (is (failure? t*))))
      (let [t (part-tick-join (sequence {:children [as ar as]}))]
        (is (= [:success :running] (yield t)))
        (is (running? t))
        (let [t* (part-tick-join (join t (success (part t))))]
          (is (= [:success :running :success] (yield t*)))
          (is (success? t*)))
        (let [t* (part-tick-join (join t (failure (part t))))]
          (is (= [:success :running] (yield t*)))
          (is (failure? t*))))
      (let [t (part-tick-join (sequence {:children [as as ar]}))]
        (is (= [:success :success :running] (yield t)))
        (is (running? t))
        (let [t* (part-tick-join (join t (failure (part t))))]
          (is (= [:success :success :running] (yield t*)))
          (is (failure? t*)))
        (let [t* (part-tick-join (join t (success (part t))))]
          (is (= [:success :success :running] (yield t*)))
          (is (success? t*)))))))

;; Parallel Tests

(deftest Parallel-construction-tests
  (testing "Parallel construction with full seed"
    (let [p (parallel {:success-th 1
                       :failure-th 1
                       :retry      1
                       :rewind-fn  #(branches (update % :retry inc)
                                              (:initial (branches %)))
                       :retry-flag FAILURE
                       :cycle-flag RUNNING
                       :flag       SUCCESS
                       :properties {:x true}
                       :parameters {:x 0 :y 0}
                       :stash      :ok
                       :children
                       [(action {:flag SUCCESS :tick-fn success})
                        (action {:tick-fn failure})
                        (action {:tick-fn running})]})]
      (is (success? p))
      (is (= 1 (peek (:cids p))))
      (is (= 1 (:success-th p)))
      (is (= 1 (:failure-th p)))
      (is (= 1 (:retry p)))
      (is (fn? (:rewind-fn p)))
      (is (= FAILURE (:retry-flag p)))
      (is (= RUNNING (:cycle-flag p)))
      (is (true? (get-in p [:properties :x])))
      (is (= {:x 0 :y 0} (params p)))
      (is (= 3 (count (branches p))))
      (is (= :ok (yield p)))
      (is (= p (parallel p)))))
  (testing "Parallel construction with minimal seed"
    (let [p (parallel {:children [(action {:tick-fn running})]})]
      (is (ready? p))
      (is (= 0 (peek (:cids p))))
      (is (nil? (:properties p)))
      (is (nil? (yield p)))
      (is (= 1 (count (branches p))))
      (is (= p (as-parallel [(action {:tick-fn running})])))
      (is (= p (parallel* (action {:tick-fn running})))))))

(defn parallel-cycle-flag-retry-flag-tests
  [cycle-flag retry-flag]
  (let [as (action {:tick-fn (tick-trace success :success)})
        af (action {:tick-fn (tick-trace failure :failure)})
        ar (action {:tick-fn (tick-trace running :running)})
        p  (parallel {:cycle-flag cycle-flag
                      :retry-flag retry-flag
                      :children   [ar ar ar]
                      :success-th 3
                      :failure-th 3})
        p3 (first (drop 3 (iterate part-tick-join1 p)))]
    (is (= cycle-flag (shift p3)))
    (is (= [:running :running :running] (yield p3)))
    (when-not (done? p3)
      (let [rc (mapv running [as af af])
            pr (first (drop 3 (iterate part-tick-join1 (branches p3 rc))))]
        (is (= retry-flag (shift pr)))
        (is (= [:running :running :running :success :failure :failure]
               (yield pr))))
      (let [sc (mapv running [as as as])
            ps (first (drop 3 (iterate part-tick-join1 (branches p3 sc))))]
        (is (success? ps))
        (is (= [:running :running :running :success :success :success]
               (yield ps))))
      (let [fc (mapv running [af af af])
            pf (first (drop 3 (iterate part-tick-join1 (branches p3 fc))))]
        (is (failure? pf))
        (is (= [:running :running :running :failure :failure :failure]
               (yield pf)))))))

(deftest Parallel-part-and-join-tests
  (testing "Parallel handover: part"
    (let [a (action {:stash :original :tick-fn identity})
          p (parallel {:stash :parental :children [a]})]
      (is (= (yield a :parental) (part p)))))
  (testing "Parallel handover: join"
    (let [a (action {:stash :original :tick-fn failure})
          p (parallel {:stash :parental :children [a]})]
      (is (success? (join p (success (part p) :ticked))))
      (is (= :ticked (yield (join p (success (part p) :ticked)))))
      (is (failure? (join p (failure (part p) :ticked))))
      (is (= :ticked (yield (join p (failure (part p) :ticked)))))
      (is (ready? (join p (running (part p) :ticked))))
      (is (= :ticked (yield (join p (running (part p) :ticked)))))))
  (testing "Multistage part & join: :cycle-flag READY, :retry-flag READY"
    (parallel-cycle-flag-retry-flag-tests READY READY))
  (testing "Multistage part & join: :cycle-flag READY, :retry-flag RUNNING"
    (parallel-cycle-flag-retry-flag-tests READY RUNNING))
  (testing "Multistage part & join: :cycle-flag READY, :retry-flag SUCCESS"
    (parallel-cycle-flag-retry-flag-tests READY SUCCESS))
  (testing "Multistage part & join: :cycle-flag READY, :retry-flag FAILURE"
    (parallel-cycle-flag-retry-flag-tests READY FAILURE))
  (testing "Multistage part & join: :cycle-flag RUNNING, :retry-flag RUNNING"
    (parallel-cycle-flag-retry-flag-tests RUNNING RUNNING))
  (testing "Multistage part & join: :cycle-flag RUNNING, :retry-flag READY"
    (parallel-cycle-flag-retry-flag-tests RUNNING READY))
  (testing "Multistage part & join: :cycle-flag RUNNING, :retry-flag SUCCESS"
    (parallel-cycle-flag-retry-flag-tests RUNNING SUCCESS))
  (testing "Multistage part & join: :cycle-flag RUNNING, :retry-flag FAILURE"
    (parallel-cycle-flag-retry-flag-tests RUNNING FAILURE)))

;; Decorator Tests

(deftest Decorator-construction-tests
  (testing "Decorator construction with full seed"
    (let [d (decorator {:branch-pred  no
                        :post-join-fn success
                        :own-tick-fn  failure
                        :branching?   false
                        :flag         SUCCESS
                        :properties   {:x true}
                        :parameters   {:x 0 :y 0}
                        :stash        :yes
                        :child        (action {:tick-fn failure})})]
      (is (success? d))
      (is (not (branch? d)))
      (is (= no (:branch-pred d)))
      (is (= success (:post-join-fn d)))
      (is (= failure (:own-tick-fn d)))
      (is (true? (get-in d [:properties :x])))
      (is (= {:x 0 :y 0} (params d)))
      (is (= :yes (yield d)))
      (is (= d (decorator d)))))
  (testing "Decorator construction with minimal seed"
    (let [d (decorator {:child (action {:tick-fn failure})})]
      (is (ready? d))
      (is (branch? d))
      (is (= yes (:branch-pred d)))
      (is (nil? (:post-join-fn d)))
      (is (= success (:own-tick-fn d)))
      (is (nil? (params d)))
      (is (nil? (yield d)))
      (is (= d (decorate (action {:tick-fn failure})))))))

(deftest Decorator-tick-part-and-join-tests
  (testing "Decorator: own tick"
    (let [d (decorator {:branch-pred no
                        :own-tick-fn success
                        :child       (action {:tick-fn failure})})]
      (is (not (branch? (tick d))))
      (is (success? (tick d)))))
  (testing "Decorator handover: part"
    (let [a (action {:tick-fn failure})
          d (decorator {:branch-pred yes
                        :own-tick-fn success
                        :stash       :parent
                        :child        a})
          t (tick d)]
      (is (branch? t))
      (is (ready? t))
      (is (= :parent (yield (part t))))))
  (testing "Decorator handover: join"
    (let [a (action {:tick-fn #(failure % :child)})
          d (decorator {:branch-pred yes
                        :own-tick-fn success
                        :stash       :parent
                        :child       a})
          t (tick d)
          j (join t (tick (part t)))]
      (is (failure? j))
      (is (= :child (yield j)))))
  (testing "Decorator handover: join and post-join"
    (let [a (action {:tick-fn #(failure % :child)})
          d (decorator {:branch-pred  yes
                        :own-tick-fn  success
                        :post-join-fn #(running % :post)
                        :stash        :parent
                        :child        a})
          t (tick d)
          j (join t (tick (part t)))]
      (is (running? j))
      (is (= :post (yield j))))))

;; Root Tests

(deftest Root-construction-tests
 (testing "Root construction with full seed"
   (let [c (condition {:pred-fn    yes
                       :flag       SUCCESS
                       :parameters {:x 1 :y 1}
                       :stash      :ok})
         r (root {:tree       c
                  :properties {:x true}
                  :parameters {:x 0 :y 0}
                  :stack      [(sequence {:children [c]})]})]
     (is (success? r))
     (is (true? (get-in r [:properties :x])))
     (is (= [(sequence {:children [c]})]
            (get r :stack)))
     (is (= {:x 0 :y 0} (params r)))
     (is (= :ok (yield r)))
     (is (= {:x true} (props r)))))
(testing "Root construction with minimal seed"
  (let [r (root {:tree (condition {:pred-fn yes})})]
    (is (ready? r))
    (is (nil? (yield r)))
    (is (nil? (params r)))
    (is (nil? (props r)))
    (is (= r (root r))))))

(deftest Root-stepping-tests
  (testing "Single leaf tree"
    (let [r (root {:tree (condition {:pred-fn yes})})
          s (step r)]
      (is (-stepper? r))
      (is (stepper? r))
      (is (finished? s))
      (is (= s (step-until r finished?)))
      (is (= s (exhaust r)))
      (is (success? s))
      (is (= r (root-of (condition {:pred-fn yes}))))))
  (testing "Single branch with three leaves"
    (let [s  (sequence {:children [(action {:tick-fn (tick-trace success :s)})
                                   (action {:tick-fn (tick-trace success :s)})
                                   (action {:tick-fn (tick-trace failure :f)})]})
          r  (root {:tree s})
          s1 (step-until r success?)
          s2 (step-until s1 success?)
          s3 (exhaust s2)]
      (is (= (branches r) (callstack r)))
      (is (= (str "Sequence [Ready]" \newline) (with-out-str (print-callstack r))))
      (is (= [:s] (yield (:tree s1))))
      (is (= (str "Sequence [Ready]" \newline "  Action [Success]" \newline)
             (with-out-str (print-callstack s1))))
      (is (= [:s :s] (yield (:tree s2))))
      (is (= [:s :s :f] (yield (:tree s3))))
      (is (= s3 (step-until r finished?)))
      (is (= (str "Sequence [Failure]" \newline)
             (with-out-str (print-callstack (step-until r finished?))))))))

(deftest Root-tick-tests
  (testing "Root, ticking when not done"
    (let [sa (action {:tick-fn (tick-trace success :s)})
          fa (action {:tick-fn (tick-trace failure :f)})
          s  (sequence {:children [(selector {:children [fa fa sa]})
                                   (parallel {:success-th 3
                                              :children   [sa sa sa]})
                                   (robin    {:summary-fn success
                                              :children   [fa fa fa]})]})
          r  (root {:tree s})
          x  (exhaust r)]
      (is (success? x))
      (is (= [:f :f :s :s :s :s :f :f :f] (yield x))))))

;; Robin Tests

(deftest Robin-construction-tests
  (testing "Robin construction with full seed"
    (let [r (robin {:retry      1
                    :rewind-fn  #(branches (update % :retry inc)
                                           (:initial (branches %)))
                    :summary-fn failure
                    :flag       SUCCESS
                    :properties {:x true}
                    :parameters {:x 0 :y 0}
                    :stash      :ok
                    :children
                    [(action {:flag SUCCESS :tick-fn success})
                     (action {:tick-fn failure})
                     (action {:tick-fn running})]})]
      (is (success? r))
      (is (= 1 (peek (:cids r))))
      (is (true? (get-in r [:properties :x])))
      (is (= 1 (:retry r)))
      (is (= failure (:summary-fn r)))
      (is (= {:x 0 :y 0} (params r)))
      (is (= 3 (count (branches r))))
      (is (= :ok (yield r)))
      (is (= r (robin r)))))
  (testing "Robin construction with minimal seed"
    (let [r (robin {:children [(action {:tick-fn running})]})]
      (is (ready? r))
      (is (= 0 (peek (:cids r))))
      (is (nil? (:properties r)))
      (is (nil? (yield r)))
      (is (nil? (params r)))
      (is (= 1 (count (branches r))))
      (is (= r (as-robin [(action {:tick-fn running})])))
      (is (= r (robin* (action {:tick-fn running})))))))

(defn robin-cycle-flag-retry-flag-tests
  [cycle-flag retry-flag]
  (let [as (action {:tick-fn (tick-trace success :success)})
        af (action {:tick-fn (tick-trace failure :failure)})
        ar (action {:tick-fn (tick-trace running :running)})
        r  (robin  {:cycle-flag cycle-flag
                    :retry-flag retry-flag
                    :children   [ar ar ar]
                    :summary-fn (fn [r]
                                  (let [res (set (take 3 (reverse (yield r))))]
                                    (condp = res
                                      #{:success} (success r)
                                      #{:failure} (failure r)
                                      (shift r READY))))})
        r3 (first (drop 3 (iterate part-tick-join1 r)))]
    (is (= cycle-flag (shift r3)))
    (is (= [:running :running :running] (yield r3)))
    (when-not (done? r3)
      (let [rc (mapv running [as af af])
            rr (first (drop 3 (iterate part-tick-join1 (branches r3 rc))))]
        (is (= retry-flag (shift rr)))
        (is (= [:running :running :running :success :failure :failure]
               (yield rr))))
      (let [sc (mapv running [as as as])
            rs (first (drop 3 (iterate part-tick-join1 (branches r3 sc))))]
        (is (success? rs))
        (is (= [:running :running :running :success :success :success]
               (yield rs))))
      (let [fc (mapv running [af af af])
            rf (first (drop 3 (iterate part-tick-join1 (branches r3 fc))))]
        (is (failure? rf))
        (is (= [:running :running :running :failure :failure :failure]
               (yield rf)))))))

(deftest Robin-part-and-join-tests
  (testing "Robin handover: part"
    (let [a (action {:stash :original :tick-fn identity})
          r (robin {:stash :parental :children [a]})]
      (is (= (yield a :parental) (part r)))))
  (testing "Robin handover: join"
    (let [a (action {:stash :original :tick-fn success})
          r (robin {:stash :parental :summary-fn success :children [a]})]
      (is (success? (join r (success (part r) :ticked))))
      (is (= :ticked (yield (join r (success (part r) :ticked)))))))
  (testing "Multistage part & join: :cycle-flag READY, :retry-flag READY"
    (robin-cycle-flag-retry-flag-tests READY READY))
  (testing "Multistage part & join: :cycle-flag READY, :retry-flag RUNNING"
    (robin-cycle-flag-retry-flag-tests READY RUNNING))
  (testing "Multistage part & join: :cycle-flag READY, :retry-flag SUCCESS"
    (robin-cycle-flag-retry-flag-tests READY SUCCESS))
  (testing "Multistage part & join: :cycle-flag READY, :retry-flag FAILURE"
    (robin-cycle-flag-retry-flag-tests READY FAILURE))
  (testing "Multistage part & join: :cycle-flag RUNNING, :retry-flag RUNNING"
    (robin-cycle-flag-retry-flag-tests RUNNING RUNNING))
  (testing "Multistage part & join: :cycle-flag RUNNING, :retry-flag READY"
    (robin-cycle-flag-retry-flag-tests RUNNING READY))
  (testing "Multistage part & join: :cycle-flag RUNNING, :retry-flag SUCCESS"
    (robin-cycle-flag-retry-flag-tests RUNNING SUCCESS))
  (testing "Multistage part & join: :cycle-flag RUNNING, :retry-flag FAILURE"
    (robin-cycle-flag-retry-flag-tests RUNNING FAILURE)))

;; Predefined placeholder nodes tests

(deftest Predefined-action-and-condition-tests
  (testing "Predefined actions"
    (is (ready? success-action))
    (is (success? (tick success-action)))
    (is (ready? failure-action))
    (is (failure? (tick failure-action)))
    (is (ready? running-action))
    (is (running? (tick running-action))))
  (testing "Predefined conditions"
    (is (ready? success-condition))
    (is (success? (tick success-condition)))
    (is (ready? failure-condition))
    (is (failure? (tick failure-condition)))))

;; Predefined helpers tests

(deftest Predefined-helpers-tests
  (testing "Stash touch and predefined flag"
    (let [us (yield (to-success inc) 0)
          uf (yield (to-failure inc) 0)
          ts (tick us)
          tf (tick uf)]
      (is (ready? us))
      (is (ready? uf))
      (is (success? ts))
      (is (failure? tf))
      (is (= (inc (yield us)) (yield ts)))
      (is (= (inc (yield uf)) (yield tf)))))
  (testing "Stash predicate to flag"
    (let [vp (via-pred identity)]
      (is (success? (tick (yield vp true))))
      (is (failure? (tick (yield vp false)))))))

(defn- m-o-m?!|multiverse<+-*%'&=:> [s] (success s))
(defn- m-o-m?!|multiverse<+-*%'&=:>-9 [s] (success s))

(deftest Friendlification-tests
  (let [subject   (friendlify m-o-m?!|multiverse<+-*%'&=:>)
        subject-2 (friendlify m-o-m?!|multiverse<+-*%'&=:>-9)]
    (is (= #?(:clj  "wikla.tree-test/m-o-m?!|multiverse<+-*%'&=:>"
              :cljs "wikla.tree-test.m-o-m?!|multiverse<+-*%'&=:>")
           subject))
    (is (= #?(:clj  "wikla.tree-test/m-o-m?!|multiverse<+-*%'&=:>-9"
              :cljs "wikla.tree-test.m-o-m?!|multiverse<+-*%'&=:>-9")
           subject-2))))

(deftest Merging-properties-and-params-tests
  (let [base-params {:a 0 :b 0}
        base-props  {:x 0 :y 0}
        a           (action {:tick-fn    success
                             :parameters base-params
                             :properties base-props})
        s           (sequence {:children [a]
                               :properties base-props})
        r           (root {:tree s
                           :parameters base-params
                           :properties base-props})
        new-params  {:a 1 :b 1 :c 1}
        new-props   {:x 1 :y 1 :z 1}
        r-full      (-> (merge-params r new-params)
                        (merge-props new-props))
        r-new-only  (-> (merge-params r new-params true)
                        (merge-props new-props true))
        part-params (into new-params base-params)
        part-props  (into new-props base-props)]
    ;; Sequence params are no-op and return nil, change nothing.
    (is (every? (partial = new-params) (keep params (node-seq r-full))))
    (is (every? (comp (partial = new-props) props) (node-seq r-full)))
    (is (every? (partial = part-params) (keep params (node-seq r-new-only))))
    (is (every? (comp (partial = part-props) props) (node-seq r-new-only)))))
