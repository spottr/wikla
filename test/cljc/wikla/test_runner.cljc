(ns wikla.test-runner
  (:require
   #?(:clj  [eftest.runner :refer        [find-tests run-tests]]
      :cljs [doo.runner    :refer-macros [doo-tests]])
   [wikla.tree-test]))

#?(:cljs (doo-tests 'wikla.tree-test))

(defn -main [& _]
  #?(:clj (run-tests (find-tests 'wikla.tree-test))))
