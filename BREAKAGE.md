# Breaking Changes Guide

_Please note: API freeze in version 1.0.0. Not there yet._

## Migration from 0.6.11 to 0.7.0

### Ticking: First Argument To `tick-fn` And Multiple Arity

The `tick-fn`s no longer gets properties as first argument, but the entire
calling node instead. Thus `:properties` must be extracted explicitly, with
entire node being expected within returned Result.

Stashless (procedural) execution was introduced, so calling a node directly
will now execute `(tick! node)` instead of `(tick node nil)`, this will require
the `tick-fn` to provide single arity entry point.

For `Condition` and `Action` extracting `:parameters` within the `tick-fn`
should not change, it has been added as node level key there. It's passed
to the constructor as before, gets extracted and put in the node.

### Status Functions Arity Changes

Single argument `success`/`failure`/`running`/`ready` no longer work on `stash`
alone, returning `Fragment`. They now want `node` as the single argument
and will return a `Result` with `nil` in `:stash`. This is to match the
procedural use of the tree (that is `(tick! node)`. `Fragment` is gone.

### Properties And Hooks Handling

Node `:properties` have been reshaped a little: hooks are now expected to rest
in a map under `:hooks` key and during their execution no exceptions are
swallowed anymore.

Hooks are now put directly on their Status keywords, with `:on-result` becoming
`:*` in this scenario. Function `wikla.core/attach-hook` streamlines this.

### No More Async

Adding `core.async` as dependency was jumping the shark. It was a mistake, that
is now rectified. No more `wikla.act`. If anybody used it, ever, feel free to
steal the code from 0.6.11.

### Misc Quality Of Life

Management of `:flag` and `:name` has been abstracted by `shift` and `label`
methods respectively. The `:parameters` field has `params` getter/setter.

## Migration from 0.7.0 to 0.8.0

### Taxonomies Added To Core Protocols

`Node/-node?` and `Status/-status?` are now used to quickly tell if a thing
is a thing of interest. Of course they have closed down implementations for
both `Object`/`object` and `nil`. Then these are used by `wikla.core/node?`
and `wikla.core/status?` Will require to be implemented in client code for
custom nodes.

### Hooks and Branches on Nodes

Nodes got reworked `Node/hooks` getter setter that can be used to get and set
hooks on the node. Works on array-map of Statuses and callables. Client code
should still use `wikla.core/attach-hook` and `wikla.core/detach-hook`.

Another addition is `Node/branches` which is a getter setter for children,
the way Zipping or Dotting would expect it -- not bound with the `Node/branch?`
predicate. Full inventory.

### Hooks Changes

Instead of `:on-<Status>` hooks are now set on literal Status keywords, with
special hook on `:*`.

#### Hooks On Robin

Hooks were shared between Robin and its summary-action. Not anymore.

### Stem and Yield on Status

`Status/stem` and `Status/yield` were added to get/set :node and :stash.
They simplify handling Results a bit. Keyword implementations will return
`nil` on get and throw exception on set.

### Slight Decorator Rework

Decorator was modified in several places: It now has :child instead
of :children, and their handling was delegated to the newly added `Node/branches`
method, causing print order change, since dot-children had different order.
Also: `:branch-cond-fn` gets the full Decorator instance now, not just
properties.

### Refs Can Be Used As Nodes

If they contain a node. They delegate the call over dereference content.

## Migration from 0.8.0 to 0.8.1

### Data fields of ExceptionInfo are now constant function wrapped

Large data structures, like non-trivial trees and huge stashes, are a bit
overwhelming when they pop-out from ExceptionInfo. Wrapping them in `constantly`
helps with display, while allows for easy access.

## Migration from 0.8.2 to 0.8.3

Shim in `wikla.core/return-node` is gone now. So is `wikla.kit/node-maker`.

`Decorator` has been rewritten and its tick functions are no longer separate
nodes. There are additional `READY` stages in its cycle.

## Migration from 0.8.3 to 0.9.0

"RIP&TEAR, because less is more."

Making the code less stupid by decreasing the LOC. Massive cuts. This release
is less about being smart and more about being compliant with source material,
delivering the core that I use, care about and would like to support further.
Everything that was not serving a specific, well defined purpose was cut.

Probably a good idea to read that [Architecture][architecture]. Since
now there is one.

Here's the big breakage list:

* `wikla.core` renamed to `wikla.tree`, all other namespaces removed.
* `Node` protocol extended with some getters-setters and parts of other
  protocols that are now gone, especially `Dot`.
* All node constructors accept a map in a shape of node. It should
  be possible to feed every node to its constructor and get an exact copy.
* `(tick node stash)` is gone, since stash is part of the node now,
  available via `(yield self)` call. So all your ticks and conditions
  need a rework. Sorry. All the status setting functions kept their
  signatures so `(success self stash)` and friends do the right thing.
* `Condition` does not have a tick function anymore, it has a predicate
  function which is `(pred-fn self) => boolean`. Then `true` is Success,
  `false` is Failure and that's it. No Ready, no Running. If you want that
  just use `Action`.
* `Selection` is gone, use `Selector` instead. Same thing, proper name.
* `Parallel` is now closer to the reference material, very similar to `Robin`,
  which is provided as alternative (summary function instead of thresholds).
* `Parallel` and `Robin` spinning by default: READY on cycle, READY on retry.
* `Ticker` is now `Root` and that's the only deep ticking node.
* No `resetable`, no `rewindable`, no `Zip`, no `Dot`. Parts of `Dot` are now
  in `Node`, zipping is done based on `branches` method from `Node`. Nodes that
  might rewind (`Parallel`, `Robin`, `Decorator`) have internal strategies
  provided for this.
* `Var`s can no longer be used as nodes, since the implementation was half
  baked at best. Improving it would involve `alter-var-root` = no.
* `Result` gone, since stash is now part of the nodes, just like the flag.
* `part` and `tick` on nodes that are `done?` returns the node immediately.
* `join` on `done?` branch gives `nil`.
* `wikla.core/all-tick` is gone, `Stepper` protocol rewritten and only
  implemented on `Root`. Nodes are not deep `tick`able anymore, removed
  directly calling `(node stash)` too.
* Removed `wikla.pre`, three predefined actions and two conditions renamed and
  added to `wikla.tree`, along with `to-success` and `to-failure`, rest is gone.
* Removed `wikla.ext`, moved reworked `Robin` to `wikla.tree`, dumped the
  rest. Bye-bye `Transaction`, `Unless`, `Until` and `Context`.
* Removed `wikla.zip` since it was obsolete due to `branches` get-set.
  Moved rewritten `zipper` to `wikla.tree`.
* Removed `wikla.dot` after rewriting DOT generation in `wikla.tree`.
* Removed `wikla.kit` since it was not general enough. Tree printing moved
  to `wikla.tree/print-tree`.
* Moved all specs to `wikla.specs` namespace. Don't need them? Don't require it.
* `wikla.tree/node-seq` can be used to extract trees (`tree-seq` based).
* Multiple checks were removed, less exception throwing. You should be fine
  as long as you run the provided machinery.
* Hooks only run during `Stepper` driven execution via `Root`, nodes no longer
  implement them directly.

Small things to be aware of:

* CLJS is now nodejs only. Not even sorry for that one.

[architecture]: ARCHITECTURE.md
