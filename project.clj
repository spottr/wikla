(defproject wikla "1.0.4-SNAPSHOT"

  :description "Behavior Tree in Clojure(Script)."

  :url "https://bitbucket.org/spottr/wikla/"

  :license {:name "MIT Public License"
            :distribution :repo
            :comments "LICENSE file in project root directory."}

  :dependencies [[org.clojure/clojure "1.11.1"]
                 [org.clojure/clojurescript "1.11.132"]]

  :profiles {:dev {:plugins [[lein-auto "0.1.3"]
                             [lein-doo "0.1.11"]
                             [lein-cljsbuild "1.1.8"]
                             [lein-codox "0.10.8"]]
                   :dependencies [[criterium "0.4.6"]
                                  [orchestra "2021.01.01-1"]
                                  [eftest "0.6.0"]
                                  [hashp "0.2.2"]]
                   :global-vars {*warn-on-reflection* false
                                 *print-namespace-maps* false}
                   :source-paths ["dev"]
                   :jvm-opts []}
             ; lein with-profile +1.9 test-clj
             :1.9 {:dependencies [[org.clojure/clojure "1.9.0"]]}
             :1.10 {:dependencies [[org.clojure/clojure "1.10.3"]]}
             :1.12 {:dependencies [[org.clojure/clojure "1.12.0-alpha5"]]}
             :uberjar {:aot :all}}

  :aot []

  :codox {:metadata {:doc/format :markdown}
          :namespaces [#"^wikla\.tree*$"]
          :exclude-vars #"(?xi)^(?:|map->.*|->[A-Z].*)$"
          :doc-files ["ARCHITECTURE.md" "CHANGELOG.md"
                      "BREAKAGE.md" "CONTRIBUTORS.md"]
          :source-uri
          ~(str "https://bitbucket.org/spottr/wikla/src/"
                "8a3a4a310bd19508a2048c2ff7001b3f3c89c06f" ; 1.0.3
                "/{filepath}#{basename}-{line}")}
  :plugins []

  :repl-options {:init-ns user}

  ;; file masks for lein-auto
  :auto {:default {:file-pattern #"\.(clj|cljs|cljc)$"}}

  ;; aliases are fun!
  :aliases {"test-all"                ["do" "test-clj," "test-cljs"]
            "test-watch"              ["auto" "test-all"]
            "test-clj"                ["do" "clean,"
                                       "run" "-m" "wikla.test-runner"]
            "test-clj-watch"          ["auto" "test-clj"]
            "test-cljs"               ["do" "doo" "node" "test-node" "once,"
                                       "clean"]
            "test-cljs-watch"         ["do" "doo" "node" "test-node" "auto,"
                                       "clean"]}

  :global-vars {}

  :source-paths ["src/cljc"]
  :test-paths ["test/cljc"]
  :omit-source false
  :target-path "target/"

  ;; output directories for CLJS test platforms
  :clean-targets ["out-test-node" :target-path]

  ;; CLJS targets
  :cljsbuild
  {:builds
   {:test-node
    {:source-paths ["src/cljc" "test/cljc"]
     :compiler     {:output-to      "out-test-node/testable.js"
                    :output-dir     "out-test-node"
                    :main           wikla.test-runner
                    :def-emits-var  true
                    :parallel-build true
                    :optimizations  :none
                    :language-out   :es-next
                    :target         :nodejs}}}})
