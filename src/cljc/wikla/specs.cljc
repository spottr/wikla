;; Copyright (c) Sławek Gwizdowski
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included
;; in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
;; IN THE SOFTWARE.
;;
(ns wikla.specs
  "Specs for wikla.tree values, nodes and callables."
  (:refer-clojure :exclude [sequence])
  (:require
   [clojure.spec.alpha :as spec]
   [wikla.tree
    :refer [node? stepper? SUCCESS FAILURE RUNNING READY queue?]]
   [clojure.spec.alpha :as s]))


;; Base values specification

(spec/def ::Node node?)
(spec/def ::Stepper stepper?)
(spec/def ::flag #{SUCCESS FAILURE RUNNING READY})
(spec/def ::parameters any?)
(spec/def ::name string?)
(spec/def ::callable ifn?)
(spec/def ::index (spec/and number? (complement neg?)))
(spec/def ::on-status #{SUCCESS FAILURE RUNNING READY :*})
(spec/def ::hooks (spec/nilable (spec/map-of ::on-status ::callable)))
(spec/def ::properties (spec/nilable (spec/keys :opt-un [::name ::hooks])))
(spec/def ::pred-fn ::callable)
(spec/def ::tick-fn ::callable)
(spec/def ::stash any?)
(spec/def ::current ::index)
(spec/def ::children (spec/coll-of ::Node :min-count 1))
(spec/def ::cids (spec/coll-of (spec/or :cid ::index :sentinel #{-1})))
(spec/def ::success-th (spec/and number? (complement neg?))) ;; allow 0
(spec/def ::failure-th (spec/and number? (complement neg?))) ;; allow 0
(spec/def ::retry ::index)
(spec/def ::retry-flag ::flag)
(spec/def ::cycle-flag ::flag)
(spec/def ::tree ::Node)
(spec/def ::stack (spec/coll-of ::Node :kind vector?))
(spec/def ::child ::Node)
(spec/def ::branch-pred ::callable)
(spec/def ::post-join-fn (spec/nilable ::callable))
(spec/def ::own-tick-fn ::callable)
(spec/def ::branching? boolean?)
(spec/def ::summary-fn (spec/nilable ::callable))

;; Node shape specification

(spec/def ::Condition
  (spec/and
   ::Node
   (spec/keys :req-un [::pred-fn ::flag ::properties ::parameters ::stash])))

(spec/def ::condition-seed
  (spec/keys :req-un [::pred-fn]
             :opt-un [::flag ::properties ::parameters ::stash]))

(spec/def ::Action
  (spec/and
   ::Node
   (spec/keys :req-un [::tick-fn ::flag ::properties ::parameters ::stash])))

(spec/def ::action-seed
  (spec/keys :req-un [::tick-fn]
             :opt-un [::flag ::properties ::parameters ::stash]))

(spec/def ::Selector
  (spec/and
   ::Node
   (spec/keys :req-un [::children ::current ::flag ::properties ::stash])))

(spec/def ::selector-seed
  (spec/keys :req-un [::children]
             :opt-un [::current ::flag ::properties ::stash]))

(spec/def ::Sequence
  (spec/and
   ::Node
   (spec/keys :req-un [::children ::current ::flag ::properties ::stash])))

(spec/def ::sequence-seed
  (spec/keys :req-un [::children]
             :opt-un [::current ::flag ::properties ::stash]))

(spec/def ::Parallel
  (spec/and
   ::Node
   (spec/keys :req-un [::children ::cids ::success-th ::failure-th ::retry
                       ::rewind-fn ::retry-flag ::cycle-flag ::flag
                       ::properties ::parameters ::stash])))

(spec/def ::parallel-seed
  (spec/keys :req-un [::children]
             :opt-un [::success-th ::failure-th ::retry ::rewind-fn
                      ::retry-flag ::cycle-flag ::flag ::properties
                      ::parameters ::stash]))

(spec/def ::Decorator
  (spec/and
   ::Node
   (spec/keys :req-un [::child ::branch-pred ::own-tick-fn ::post-join-fn
                       ::branching? ::flag ::properties ::parameters
                       ::stash])))

(spec/def ::decorator-seed
  (spec/keys :req-un [::child]
             :opt-un [::branch-pred ::own-tick-fn ::post-join-fn
                      ::branching? ::flag ::properties
                      ::parameters ::stash]))

(spec/def ::Root
  (spec/and
   ::Node
   ::Stepper
   (spec/keys :req-un [::tree ::properties ::stack])))

(spec/def ::root-seed
  (spec/keys :req-un [::tree]
             :opt-un [::properties ::stack]))

(spec/def ::Robin
  (spec/and
   ::Node
   (spec/keys :req-un [::children ::cids ::retry ::rewind-fn ::summary-fn
                       ::retry-flag ::cycle-flag ::flag ::properties
                       ::parameters ::stash])))

(spec/def ::robin-seed
  (spec/keys :req-un [::children]
             :opt-un [::retry ::rewind-fn ::summary-fn ::retry-flag
                      ::cycle-flag ::flag ::properties ::parameters
                      ::stash]))

;; API specification

(spec/fdef wikla.tree/success?
  :args (spec/cat :node ::Node) :ret boolean?)

(spec/fdef wikla.tree/success
  :args (spec/alt :1-arg  (spec/cat :node ::Node)
                  :2-args (spec/cat :node ::Node :stash any?))
  :ret ::Node)

(spec/fdef wikla.tree/failure?
  :args (spec/cat :node ::Node) :ret boolean?)

(spec/fdef wikla.tree/failure
  :args (spec/alt :1-arg  (spec/cat :node ::Node)
                  :2-args (spec/cat :node ::Node :stash any?))
  :ret ::Node)

(spec/fdef wikla.tree/running?
  :args (spec/cat :node ::Node) :ret boolean?)

(spec/fdef wikla.tree/running
  :args (spec/alt :1-arg  (spec/cat :node ::Node)
                  :2-args (spec/cat :node ::Node :stash any?))
  :ret ::Node)

(spec/fdef wikla.tree/ready?
  :args (spec/cat :node ::Node) :ret boolean?)

(spec/fdef wikla.tree/ready
  :args (spec/alt :1-arg  (spec/cat :node ::Node)
                  :2-args (spec/cat :node ::Node :stash any?))
  :ret ::Node)

(spec/fdef wikla.tree/done?
  :args (spec/cat :node ::Node) :ret boolean?)

(spec/fdef wikla.tree/decided?
  :args (spec/cat :node ::Node) :ret boolean?)

(spec/fdef wikla.tree/attach-hook
  :args (spec/alt :3-args (spec/cat :node      ::Node
                                    :on-status ::on-status
                                    :hook-fn   ::callable)
                  :2-args (spec/cat :node      ::Node
                                    :hook-fn   ::callable))
  :ret ::Node)

(spec/fdef wikla.tree/detach-hook
  :args (spec/alt :2-args (spec/cat :node      ::Node
                                    :on-status ::on-status)
                  :1-arg  (spec/cat :node      ::Node))
  :ret ::Node)

(spec/fdef wikla.tree/run-hooks!
  :args (spec/cat :node ::Node) :ret ::Node)

;; Useful Things

(spec/fdef wikla.tree/zipper :args (spec/cat :node ::Node) :ret any?)

(spec/fdef wikla.tree/print-tree
  :args (spec/alt :2-args (spec/cat :mark-pred  ::callable
                                    :start-node ::Node)
                  :1-args (spec/cat :start-node ::Node))
  :ret  nil?)

(spec/fdef wikla.tree/print-callstack
  :args (spec/cat :a-root ::Stepper)
  :ret  nil?)

(spec/fdef wikla.tree/dtx
  :args (spec/cat :a-root ::Stepper) :ret ::Stepper)

(spec/fdef wikla.tree/node-seq
  :args (spec/cat :node ::Node)
  :ret  (spec/coll-of ::Node))

(spec/fdef wikla.tree/yes :args (spec/* any?) :ret true?)

(spec/fdef wikla.tree/no :args (spec/* any?) :ret false?)

(spec/fdef wikla.tree/mu :args (spec/* any?) :ret nil?)

(spec/fdef wikla.tree/in-stash
  :args (spec/alt :2-args (spec/cat :node ::Node
                                    :f    ::callable)
                  :n-args (spec/cat :node ::Node
                                    :f    ::callable
                                    :var  (spec/* any?)))
  :ret  ::Node)

(spec/fdef wikla.tree/in-props
  :args (spec/alt :2-args (spec/cat :node ::Node
                                    :f    ::callable)
                  :n-args (spec/cat :node ::Node
                                    :f    ::callable
                                    :var  (spec/* any?)))
  :ret  ::Node)

(spec/fdef wikla.tree/in-params
  :args (spec/alt :2-args (spec/cat :node ::Node
                                    :f    ::callable)
                  :n-args (spec/cat :node ::Node
                                    :f    ::callable
                                    :var  (spec/* any?)))
  :ret  ::Node)


(spec/fdef wikla.tree/softer-merge
  :args (spec/cat :target map? :source map?)
  :ret map?)

(spec/fdef wikla.tree/merge-props
  :args (spec/alt :2-args (spec/cat :sub-tree      ::Node
                                    :new-props     (s/nilable map?))
                  :3-args (spec/cat :sub-tree      ::Node
                                    :new-props     (s/nilable map?)
                                    :missing-only? boolean?)))

(spec/fdef wikla.tree/merge-params
  :args (spec/alt :2-args (spec/cat :sub-tree      ::Node
                                    :new-params    (s/nilable map?))
                  :3-args (spec/cat :sub-tree      ::Node
                                    :new-params    (s/nilable map?)
                                    :missing-only? boolean?)))

(spec/fdef wikla.tree/friendlify
  :args (spec/cat :something any?)
  :ret  string?)

(spec/fdef wikla.tree/queue?
  :args (spec/cat :o any?)
  :ret  boolean?)

(spec/fdef wikla.tree/queue
  :args (spec/cat :o seqable?)
  :ret  queue?)

;; Constructors

(spec/fdef wikla.tree/condition
  :args  (spec/cat :seed ::condition-seed)
  :ret  ::Condition)

(spec/fdef wikla.tree/fn->condition
  :args (spec/alt :1-arg  (spec/cat :pred-fn ::pred-fn)
                  :2-args (spec/cat :name ::name :pred-fn ::pred-fn))
  :ret  ::Condition)

(spec/fdef wikla.tree/action
  :args  (spec/cat :seed ::action-seed)
  :ret  ::Action)

(spec/fdef wikla.tree/fn->action
  :args  (spec/alt :1-arg  (spec/cat :tick-fn ::tick-fn)
                   :2-args (spec/cat :name ::name :tick-fn ::tick-fn))
  :ret  ::Action)

(spec/fdef wikla.tree/selector
  :args (spec/cat :seed ::selector-seed)
  :ret  ::Selector)

(spec/fdef wikla.tree/as-selector
  :args (spec/cat :children ::children)
  :ret  ::Selector)

(spec/fdef wikla.tree/selector*
  :args (spec/cat :nodes (spec/* ::Node))
  :ret  ::Selector)

(spec/fdef wikla.tree/sequence
  :args (spec/cat :seed ::sequence-seed)
  :ret  ::Sequence)

(spec/fdef wikla.tree/as-sequence
  :args (spec/cat :children ::children)
  :ret  ::Sequence)

(spec/fdef wikla.tree/parallel
  :args (spec/cat :seed ::parallel-seed)
  :ret  ::Parallel)

(spec/fdef wikla.tree/as-parallel
  :args (spec/cat :children ::children)
  :ret  ::Parallel)

(spec/fdef wikla.tree/parallel*
  :args (spec/cat :nodes (spec/* ::Node))
  :ret  ::Parallel)

(spec/fdef wikla.tree/decorator
  :args (spec/cat :seed ::decorator-seed)
  :ret  ::Decorator)

(spec/fdef wikla.tree/decorate
  :args (spec/cat :child ::Node)
  :ret  ::Decorator)

(spec/fdef wikla.tree/root
  :args (spec/cat :seed ::root-seed)
  :ret  ::Root)

(spec/fdef wikla.tree/root-of
  :args (spec/cat :tree ::Node)
  :ret  ::Root)

;; Extra Nodes

(spec/fdef wikla.tree/robin
  :args (spec/cat :seed ::robin-seed)
  :ret  ::Robin)

(spec/fdef wikla.tree/as-robin
  :args (spec/cat :children ::children)
  :ret  ::Robin)

(spec/fdef wikla.tree/robin*
  :args (spec/cat :nodes (spec/* ::Node))
  :ret  ::Robin)

;; Predefined helpers

(spec/fdef wikla.tree/to-success
  :args (spec/cat :a-fn ::callable)
  :ret  ::Action)

(spec/fdef wikla.tree/to-failure
  :args (spec/cat :a-fn ::callable)
  :ret  ::Action)

(spec/fdef wikla.tree/via-pred
  :args (spec/cat :pred ::callable)
  :ret  ::Condition)

;; DOTs

(spec/fdef wikla.tree/dot-string
  :args (spec/cat :node ::Node)
  :ret  string?)

#?(:clj
   (spec/fdef wikla.tree/->dot
     :args (spec/cat :node ::Node :file-name string?)
     :ret  true?))

#?(:clj
   (spec/fdef wikla.tree/->svg
     :args (spec/cat :tree ::Dot :file-name string?)
     :ret  boolean?))

#?(:clj
   (spec/fdef wikla.tree/->png
     :args (spec/cat :tree ::Dot :file-name string?)
     :ret  boolean?))
