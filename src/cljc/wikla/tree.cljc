;; Copyright (c) Sławek Gwizdowski
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included
;; in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
;; IN THE SOFTWARE.
;;
(ns wikla.tree
  "Behavior Tree branches, leaves and tools."
  (:refer-clojure :exclude [sequence])
  (:require
   [clojure.string :as string]
   [clojure.zip :as zip]
   #?(:clj [clojure.java.shell :refer [sh]])))

(defprotocol Node
  "Branching and ticking, access and viz helpers, just node things."
  (-node? [_] "Is this a Node?")
  (shift [this] [this flag*] "Get-set flag.")
  (label [this] [this label*] "Get-set name.")
  (hooks [this] [this hook*] "Get-set hooks.")
  (props [this] [this props*] "Get-set properties.")
  (params [this] [this params*] "Get-set parameters.")
  (branches [this] [this children*] "Get-set children.")
  (stem [this] [this stem*] "Get-set node.")
  (yield [this] [this yield*] "Get-set stash.")
  (branch? [this] "Is this a branch?")
  (part [this] "Get next child.")
  (join [this child*] "Join current child.")
  (tick [this] "Update node.")
  (dot-shape [this] "Desired Shape in the visualization.")
  (dot-label [this] "Node Description in the visualization.")
  (dot-color [this] "Node color in the visualization."))

(defprotocol Stepper
  "Step-by-step ticking."
  (-stepper? [this] "Is this a Stepper?")
  (finished? [this] "Is this tick completed?.")
  (handle-branch [this] "Perform branch op.")
  (handle-leaf [this] "Perform leaf op.")
  (step [this] "Perform single op.")
  (step-until [this pred] "Perform steps until predicate is true.")
  (callstack [this] "Return full stack from root to current node.")
  (exhaust [this] "Step through tree until finished."))

(extend-type nil
  Node
  (-node? [_] false)
  Stepper
  (-stepper? [_] false))

(extend-type #?(:clj Object :cljs object)
  Node
  (-node? [_] false)
  Stepper
  (-stepper? [_] false))

(def ^{:const true} SUCCESS :Success)
(def ^{:const true} FAILURE :Failure)
(def ^{:const true} RUNNING :Running)
(def ^{:const true} READY   :Ready)
(def ^{:const true}
  dot-colors
  {SUCCESS "limegreen"
   FAILURE "orangered"
   RUNNING "dodgerblue"
   READY   "gainsboro"})

(defn node?
  "Is this a Node?"
  [o] (try (-node? o) (catch #?(:clj Exception :cljs :default) _ false)))

(defn stepper?
  "Is this a Stepper?"
  [o] (try (-stepper? o) (catch #?(:clj Exception :cljs :default) _ false)))

;; Status get-set

(defn success?
  "Is node shifted to SUCCESS?"
  [node]
  #?(:clj  (identical? (shift node) SUCCESS)
     :cljs (keyword-identical? (shift node) SUCCESS)))

(defn success
  "Shift node to SUCCESS. Optionally with stash."
  ([node]
   (shift node SUCCESS))
  ([node stash]
   (yield (shift node SUCCESS) stash)))

(defn failure?
  "Is node shifted to FAILURE?"
  [node]
  #?(:clj  (identical? (shift node) FAILURE)
     :cljs (keyword-identical? (shift node) FAILURE)))

(defn failure
  "Shift node to FAILURE. Optionally with stash."
  ([node]
   (shift node FAILURE))
  ([node stash]
   (yield (shift node FAILURE) stash)))

(defn running?
  "Is node shifted to RUNNING?"
  [node]
  #?(:clj  (identical? (shift node) RUNNING)
     :cljs (keyword-identical? (shift node) RUNNING)))

(defn running
  "Shift node to RUNNING. Optionally with stash."
  ([node]
   (shift node RUNNING))
  ([node stash]
   (yield (shift node RUNNING) stash)))

(defn ready?
  "Is node shifted to READY?"
  [node]
  #?(:clj  (identical? (shift node) READY)
     :cljs (keyword-identical? (shift node) READY)))

(defn ready
  "Shift node to READY. Optionally with stash."
  ([node]
   (shift node READY))
  ([node stash]
   (yield (shift node READY) stash)))

(defn done?
  "Is node shifted to SUCCESS or FAILURE?"
  [node]
  (let [f (shift node)]
    #?(:clj  (or (identical? f SUCCESS)
                 (identical? f FAILURE))
       :cljs (or (keyword-identical? f SUCCESS)
                 (keyword-identical? f FAILURE)))))

(defn decided?
  "Is node shifted to SUCCESS, FAILURE or RUNNING?"
  [node]
  (let [f (shift node)]
    #?(:clj  (or (identical? f SUCCESS)
                 (identical? f FAILURE)
                 (identical? f RUNNING))
       :cljs (or (keyword-identical? f SUCCESS)
                 (keyword-identical? f FAILURE)
                 (keyword-identical? f RUNNING)))))

;; Hooks

(defn attach-hook
  "Attach hook on SUCCESS, FAILURE, RUNNING, READY or :* key.

   Hook function should have the signature:

       (hook-fn node') => nil"
  ([node on-status hook-fn]
   (hooks node (assoc (hooks node) on-status hook-fn)))
  ([node hook-fn]
   (hooks node (assoc (hooks node) :* hook-fn))))

(defn detach-hook
  "Detach hook on SUCCESS, FAILURE, RUNNING, READY or :* key."
  ([node on-status]
   (let [hs (hooks node)]
     (if (contains? hs on-status)
       (hooks node (dissoc hs on-status))
       node)))
  ([node]
   (let [hs (hooks node)]
     (if (contains? hs :*)
       (hooks node (dissoc hs :*))
       node))))

(defn run-hooks!
  "Try to execute hooks for side-effects, return node."
  [node]
  (when-let [hs (hooks node)]
    (when (seq hs)
      (when-let [status-hook-fn (get hs (shift node))]
        (status-hook-fn node))
      (when-let [result-hook-fn (get hs :*)]
        (result-hook-fn node))))
  node)

;; Useful things

(defn zipper
  "Eat node, return zipper."
  [node]
  (zip/zipper (comp seq branches) branches branches node))

(defn print-tree
  "Print nodes of a tree, indented by depth, marked via predicate."
  ([marker-pred start-node]
   (letfn [(flattie [loc]
             (lazy-seq
              (when-not (zip/end? loc)
                (let [n (zip/node loc)]
                  (cons [(count (zip/path loc))
                         (dot-label n)
                         (name (shift n))
                         (marker-pred n)]
                        (flattie (zip/next loc)))))))]
     (doseq [[depth label flag mark] (flattie (zipper start-node))]
       (println (str (if mark "*" " ")
                     (apply str (repeat depth "  "))
                     label " [" flag "]")))))
  ([start-node]
   (print-tree (constantly false) start-node)))

(defn print-callstack
  "Print callstack from given a Stepper implementing callstack."
  [a-root]
  (doseq [[depth node] (map vector (range) (callstack a-root))]
    (println (str (apply str (repeat depth "  "))
                  (dot-label node)
                  " [" (name (shift node)) "]"))))

(defn dtx
  "Run Stepper until Exception is thrown, rethrows with one step prior.

  :root key of ExceptionInfo holds a callable containing throwing step."
  [a-root]
  (let [next-step (try (step a-root)
                       (catch #?(:clj Exception :cljs :default) ex
                         (throw (ex-info "Exception in next step!"
                                         {:root (constantly a-root)}
                                         ex))))]
    (if (= a-root next-step) ;; step is stable, halt on this
      next-step
      (recur next-step))))

(defn node-seq
  "Eat node, return seq."
  [node]
  (tree-seq (comp seq branches) branches node))

(defn yes
  "Always true, just like (constantly true), except interned."
  [& _] true)

(defn no
  "Always false, just like (constantly false), except interned."
  [& _] false)

(defn mu
  "Always nil, just like (constantly nil), except interned."
  [& _] nil)

(defn in-stash
  "Extract stash from node, feed it to function f, put it back in."
  ([node f]
   (yield node (f (yield node))))
  ([node f x]
   (yield node (f (yield node) x)))
  ([node f x y]
   (yield node (f (yield node) x y)))
  ([node f x y z]
   (yield node (f (yield node) x y z)))
  ([node f x y z & more]
   (yield node (apply f (yield node) x y z more))))

(defn in-props
  "Extract properties from node, feed them to function f, put them back in."
  ([node f]
   (props node (f (props node))))
  ([node f x]
   (props node (f (props node) x)))
  ([node f x y]
   (props node (f (props node) x y)))
  ([node f x y z]
   (props node (f (props node) x y z)))
  ([node f x y z & more]
   (props node (apply f (props node) x y z more))))

(defn in-params
  "Extract parameters from node, feed them to function f, put them back in."
  ([node f]
   (params node (f (params node))))
  ([node f x]
   (params node (f (params node) x)))
  ([node f x y]
   (params node (f (params node) x y)))
  ([node f x y z]
   (params node (f (params node) x y z)))
  ([node f x y z & more]
   (params node (apply f (params node) x y z more))))

(defn- filler-merge
  "Only merge keys from updates map that do not exist in target map."
  [target updates]
  (-> (reduce-kv (fn kv-reducer [res k v]
                   (if (contains? res k) res (assoc! res k v)))
                 (transient target)
                 updates)
      (persistent!)))

(defn merge-props
  "Zip down sub-tree and update properties from new-props map.

  Set missing-only? to not overwrite existing values. Defaults to false."
  ([sub-tree new-props missing-only?]
   (if (empty? new-props)
     sub-tree
     (let [mstrat (fnil (if missing-only? filler-merge into) {})]
       (loop [z (-> (zipper sub-tree)
                    (zip/edit in-props mstrat new-props))]
         (if (zip/end? z)
           (zip/root z)
           (->> (zip/edit z in-props mstrat new-props)
                (zip/next)
                (recur)))))))
  ([sub-tree new-props]
   (merge-props sub-tree new-props false)))

(defn merge-params
  "Zip down sub-tree and update properties from new-props map.

  Set missing-only? to not overwrite existing values. Defaults to false."
  ([sub-tree new-params missing-only?]
   (if (empty? new-params)
     sub-tree
     (let [mstrat (fnil (if missing-only? filler-merge into) {})]
       (loop [z (-> (zipper sub-tree)
                    (zip/edit in-params mstrat new-params))]
         (if (zip/end? z)
           (zip/root z)
           (->> (zip/edit z in-params mstrat new-params)
                (zip/next)
                (recur)))))))
  ([sub-tree new-params]
   (merge-params sub-tree new-params false)))

(defn friendlify
  "Try to get a friendlier function name.

  In Clojure class is turned into String, then processed with regexps.

  In ClojureScript string is expected to start with 'function' followed
  by name that will be extracted and processed in similar fashion. Fallback
  returns 48 starting characters of the stringified object."
  [a-something]
  (letfn [(common-replacements [s]
            (-> (string/replace s "_BANG_" "!")
                (string/replace "_BAR_" "|")
                (string/replace "_SHARP_" "#")
                (string/replace "_PERCENT_" "%")
                (string/replace "_AMPERSAND_" "&")
                (string/replace "_QMARK_" "?")
                (string/replace "_PLUS_" "+")
                (string/replace "_STAR_" "*")
                (string/replace "_LT_" "<")
                (string/replace "_GT_" ">")
                (string/replace "_EQ_" "=")
                (string/replace "_DOT_" ".")
                (string/replace "_COLON_" ":")
                (string/replace "_SINGLEQUOTE_" "'")
                (string/replace \_ \-)))]
    #?(:clj
       (-> a-something
           (class)
           (str)
           (common-replacements)
           (string/replace-first #"(^class )" "")
           (string/replace-first #"((?:--\d+|)@[a-fA-F0-9]+$)" "")
           (string/replace-first #"(--\d+$)" "")
           (string/replace \$ \/))
       :cljs
       (let [s (string/replace (str a-something) \newline \;)]
         (if (re-find #"^function\s.*" s)
           (-> (string/replace s #"^function\s*([^(\s]+).*" "$1")
               (string/replace \$ \.)
               (common-replacements))
           (re-find #".{1,48}" s))))))

(def persistent-queue
  "Empty PersistentQueue."
  #?(:clj  clojure.lang.PersistentQueue/EMPTY
     :cljs cljs.core/PersistentQueue.EMPTY))

(defn queue?
  "Is object o a instance of PersistentQueue?"
  [o]
  (instance? #?(:clj  clojure.lang.PersistentQueue
                :cljs cljs.core/PersistentQueue)
             o))

(defn queue
  "Make object o a PersistentQueue."
  [o] (if (queue? o) o (into persistent-queue o)))

;; Nodes

(defrecord Action [tick-fn flag properties parameters stash]
  Node
  (-node? [_] true)
  (shift [_] flag)
  (shift [this flag*] (assoc this :flag flag*))
  (label [_] (:name properties ""))
  (label [this label*] (assoc-in this [:properties :name] label*))
  (hooks [_] (:hooks properties))
  (hooks [this hooks*] (assoc-in this [:properties :hooks] hooks*))
  (props [_] properties)
  (props [this props*] (assoc this :properties props*))
  (params [_] parameters)
  (params [this params*] (assoc this :parameters params*))
  (branches [_] nil)
  (branches [this _] this)
  (stem [this] this)
  (stem [_ stem*] (yield stem* stash))
  (yield [_] stash)
  (yield [this yield*] (assoc this :stash yield*))
  (branch? [_] false)
  (tick [this]
    (if (done? this)
      this
      (tick-fn this)))
  (dot-shape [_] "rect")
  (dot-label [_]
    (let [n (str (:name properties))]
      (if (seq n) (str "Action: " n) "Action")))
  (dot-color [this]
    (get dot-colors (shift this) (get dot-colors READY))))

(defn action
  "Create an Action node from given seed, which must at least
   contain :tick-fn.

  Expectation regarding the tick-fn:

      (tick-fn node) => node'"
  [seed]
  (Action. (get seed :tick-fn)
           (get seed :flag READY)
           (get seed :properties)
           (get seed :parameters)
           (get seed :stash)))

(defn fn->action
  "Create Action out of a tick-fn function."
  ([tick-fn]
   (action {:properties {:name (friendlify tick-fn)}
            :tick-fn    tick-fn}))
  ([name tick-fn]
   (action {:properties {:name name} :tick-fn tick-fn})))

(defrecord Condition [pred-fn flag properties parameters stash]
  Node
  (-node? [_] true)
  (shift [_] flag)
  (shift [this flag*] (assoc this :flag flag*))
  (label [_] (:name properties ""))
  (label [this label*] (assoc-in this [:properties :name] label*))
  (hooks [_] (:hooks properties))
  (hooks [this hooks*] (assoc-in this [:properties :hooks] hooks*))
  (props [_] properties)
  (props [this props*] (assoc this :properties props*))
  (params [_] parameters)
  (params [this params*] (assoc this :parameters params*))
  (branches [_] nil)
  (branches [this _] this)
  (stem [this] this)
  (stem [_ stem*] (yield stem* stash))
  (yield [_] stash)
  (yield [this yield*] (assoc this :stash yield*))
  (branch? [_] false)
  (tick [this]
    (if (done? this)
      this
      (if (pred-fn this)
        (success this)
        (failure this))))
  (dot-shape [_] "rect")
  (dot-label [_]
    (let [n (str (:name properties))]
      (if (seq n) (str "Condition: " n) "Condition")))
  (dot-color [this]
    (get dot-colors (shift this) (get dot-colors READY))))

(defn condition
  "Create a Condition node from given seed, which must at least
   contain :pred-fn.

  Expectation regarding the pred-fn:

      (pred-fn node) => Boolean-ish"
  [seed]
  (Condition. (get seed :pred-fn)
              (get seed :flag READY)
              (get seed :properties)
              (get seed :parameters)
              (get seed :stash)))

(defn fn->condition
  "Create Condition out of a pred-fn function."
  ([pred-fn]
   (condition {:properties {:name (friendlify pred-fn)}
               :pred-fn    pred-fn}))
  ([name pred-fn]
   (condition {:properties {:name name} :pred-fn pred-fn})))

(defrecord Selector [children current flag properties stash]
  Node
  (-node? [_] true)
  (shift [_] flag)
  (shift [this flag*] (assoc this :flag flag*))
  (label [_] (:name properties ""))
  (label [this label*] (assoc-in this [:properties :name] label*))
  (hooks [_] (:hooks properties))
  (hooks [this hooks*] (assoc-in this [:properties :hooks] hooks*))
  (props [_] properties)
  (props [this props*] (assoc this :properties props*))
  (params [_] nil)
  (params [this _] this)
  (branches [_] children)
  (branches [this children*] (assoc this :children (vec children*)))
  (stem [this] this)
  (stem [_ stem*] (yield stem* stash))
  (yield [_] stash)
  (yield [this yield*] (assoc this :stash yield*))
  (branch? [_] true)
  (part [this]
    (when-not (done? this)
      (yield (get children current) stash)))
  (join [this child*]
    (if (done? this)
      this
      (let [this* (assoc (assoc-in this [:children current] child*)
                         :stash (yield child*))]
        (cond
          (running? child*)
          (assoc this* :flag RUNNING)
          (success? child*)
          (assoc this* :flag SUCCESS)
          (failure? child*)
          (if (= current (dec (count children)))
            (assoc this* :flag FAILURE)
            (assoc (update this* :current inc) :flag READY))
          :else
          (throw (ex-info "Could not join child in Selector!"
                          {:selector (constantly this*)}))))))
  (dot-shape [_] "rect")
  (dot-label [_]
    (let [n (str (:name properties))]
      (if (seq n) (str "Selector: " n) "Selector")))
  (dot-color [this]
    (get dot-colors (shift this) (get dot-colors READY))))

(defn selector
  "Create a Selector node from given seed, which must at least
   contain :children.

  Sequence will evalute to:

  * SUCCESS if a child returns SUCCESS
  * FAILURE if all children ran and resulted in FAILURE
  * RUNNING immediately if any child is RUNNING."
  [seed]
  (Selector. (vec (get seed :children))
             (get seed :current 0)
             (get seed :flag READY)
             (get seed :properties)
             (get seed :stash)))

(defn as-selector
  "Create a Selector from collection of nodes."
  [coll]
  (selector {:children coll}))

(defn selector*
  "Create a Selector from node arguments."
  [& nodes]
  (selector {:children nodes}))

(defrecord Sequence [children current flag properties stash]
  Node
  (-node? [_] true)
  (shift [_] flag)
  (shift [this flag*] (assoc this :flag flag*))
  (label [_] (:name properties ""))
  (label [this label*] (assoc-in this [:properties :name] label*))
  (hooks [_] (:hooks properties))
  (hooks [this hooks*] (assoc-in this [:properties :hooks] hooks*))
  (props [_] properties)
  (props [this props*] (assoc this :properties props*))
  (params [_] nil)
  (params [this _] this)
  (branches [_] children)
  (branches [this children*] (assoc this :children (vec children*)))
  (stem [this] this)
  (stem [_ stem*] (yield stem* stash))
  (yield [_] stash)
  (yield [this yield*] (assoc this :stash yield*))
  (branch? [_] true)
  (part [this]
    (when-not (done? this)
      (yield (get children current) stash)))
  (join [this child*]
    (if (done? this)
      this
      (let [this* (assoc (assoc-in this [:children current] child*)
                         :stash (yield child*))]
        (cond
          (running? child*)
          (assoc this* :flag RUNNING)
          (failure? child*)
          (assoc this* :flag FAILURE)
          (success? child*)
          (if (= current (dec (count children)))
            (assoc this* :flag SUCCESS)
            (assoc (update this* :current inc) :flag READY))
          :else
          (throw (ex-info "Could not join child in Sequence!"
                          {:sequence (constantly this*)}))))))
  (dot-shape [_] "rect")
  (dot-label [_]
    (let [n (str (:name properties))]
      (if (seq n) (str "Sequence: " n) "Sequence")))
  (dot-color [this]
    (get dot-colors (shift this) (get dot-colors READY))))

(defn sequence
  "Create a Sequence node from given seed which must at least
   contain :children.

  Sequence will evalute to:

  * SUCCESS if all children ran and were SUCCESS
  * FAILURE immediately if any child is FAILURE
  * RUNNING immediately if any child is RUNNING."
  [seed]
  (Sequence. (vec (get seed :children))
             (get seed :current 0)
             (get seed :flag READY)
             (get seed :properties)
             (get seed :stash)))

(defn as-sequence
  "Create a Sequence from collection of nodes."
  [coll]
  (sequence {:children coll}))

(defn sequence*
  "Create a Sequence from node arguments."
  [& nodes]
  (sequence {:children nodes}))

(defrecord Parallel [children cids success-th failure-th retry rewind-fn
                     retry-flag cycle-flag flag properties parameters stash]
  Node
  (-node? [_] true)
  (shift [_] flag)
  (shift [this flag*] (assoc this :flag flag*))
  (label [_] (:name properties ""))
  (label [this label*] (assoc-in this [:properties :name] label*))
  (props [_] properties)
  (props [this props*] (assoc this :properties props*))
  (params [_] parameters)
  (params [this params*] (assoc this :parameters params*))
  (hooks [_] (:hooks properties))
  (hooks [this hooks*] (assoc-in this [:properties :hooks] hooks*))
  (branches [_] children)
  (branches [this children*]
    (let [nodes (vec children*)
          cids  (-> (queue (filter (comp not done? (partial get nodes))
                                   (range (count nodes))))
                    (conj -1))]
      (assoc this :children (vary-meta nodes assoc :initial nodes) :cids cids)))
  (stem [this] this)
  (stem [_ stem*] (yield stem* stash))
  (yield [_] stash)
  (yield [this yield*] (assoc this :stash yield*))
  (branch? [_] true)
  (part [this]
    (when-not (done? this)
      (yield (get children (peek cids)) stash)))
  (join [this child*]
    (if (done? this)
      this
      (let [children* (assoc children (peek cids) child*)
            cids*     (if (running? child*)
                        (conj (pop cids) (peek cids))
                        (pop cids))
            this*     (assoc this :children children*
                             :cids cids*
                             :stash (yield child*))
            next*     (peek cids*)]
        (if (not= -1 next*) ;; not end of cycle
          (assoc this* :flag READY)
          (if (not= 1 (count cids*)) ;; not all children done
            (assoc this* :flag cycle-flag :cids (conj (pop cids*) next*))
            (if (<= success-th (count (filterv success? children*)))
              (assoc this* :flag SUCCESS)
              (if (<= failure-th (count (filterv failure? children*)))
                (assoc this* :flag FAILURE)
                (let [this** (assoc this* :flag retry-flag)]
                  (if (done? this**)
                    this**
                    (if (nil? rewind-fn)
                      (branches (update this** :retry inc)
                                (:initial (meta children)))
                      (rewind-fn (update this** :retry inc))))))))))))
  (dot-shape [_] "rect")
  (dot-label [_]
    (let [n (str (:name properties))
          l (str "Parallel (S:" success-th " F:" failure-th " R:" retry ")")]
      (if (seq n)
        (str l ": " n) l)))
  (dot-color [this]
    (get dot-colors (shift this) (get dot-colors READY))))

(defn parallel
  "Create a Parallel node with given seed and nodes. Children will
  run in order, child that is RUNNING will be put in queue and Parallel will
  get READY to execute next child immediately. Parallel will itself return
  READY on retry and full cycle.

  When all children are either SUCCESS or FAILURE final tally happens.
  If no threshold is reached, then fresh copy of children will be brought
  back for execution and Parallel will raise retry count and retry.

  Parallel will evalute to, in order:

  * SUCCESS if all children are done and success-th was reached
  * FAILURE if all children are done and failure-th was reached
  * cycle-flag on cycle completion, READY by default
  * retry-flag on retry initiation, READY by default.
   
  If both success-th and failure-th were reached SUCCESS will be returned.

  The rewind-fn is given Parallel to prepare it for re-run, defaults to nil,
  in which case fresh copy of children is used. The rewind-fn can overwrite
  retry-flag, which is set before that function runs.

  For alternate Parallel implementation see: Robin."
  ([seed]
   (branches (Parallel. []
                        nil
                        (get seed :success-th 1)
                        (get seed :failure-th 1)
                        (get seed :retry 0)
                        (get seed :rewind-fn)
                        (get seed :retry-flag READY)
                        (get seed :cycle-flag READY)
                        (get seed :flag READY)
                        (get seed :properties)
                        (get seed :parameters)
                        (get seed :stash))
             (get seed :children))))

(defn as-parallel
  "Create a Parallel from collection of nodes."
  [coll]
  (parallel {:children coll}))

(defn parallel*
  "Create a Parallel from node arguments."
  [& nodes]
  (parallel {:children nodes}))

(defrecord Decorator [child branch-pred own-tick-fn post-join-fn branching?
                      flag properties parameters stash]
  Node
  (-node? [_] true)
  (shift [_] flag)
  (shift [this flag*] (assoc this :flag flag*))
  (label [_] (:name properties ""))
  (label [this label*] (assoc-in this [:properties :name] label*))
  (hooks [_] (:hooks properties))
  (hooks [this hooks*] (assoc-in this [:properties :hooks] hooks*))
  (props [_] properties)
  (props [this props*] (assoc this :properties props*))
  (params [_] parameters)
  (params [this params*] (assoc this :parameters params*))
  (branches [_] [child])
  (branches [this children*]
    (let [child (first children*)]
      (assoc this :child (vary-meta child assoc :initial child))))
  (stem [this] this)
  (stem [_ stem*] (yield stem* stash))
  (yield [_] stash)
  (yield [this yield*] (assoc this :stash yield*))
  (branch? [_] branching?)
  (tick [this]
    (if (done? this)
      this
      (if-not (ready? child)
        (if (nil? post-join-fn)
          (assoc this :flag (shift child))
          (post-join-fn this))
        (if (branch-pred this)
          (assoc this :branching? true :flag READY)
          (own-tick-fn (assoc this :branching? false))))))
  (part [this]
    (when (and branching? (not (done? this)))
      (yield child stash)))
  (join [this child*]
    (if (done? this)
      this
      (if (nil? post-join-fn)
        (assoc this :child child* :stash (yield child*) :flag (shift child*))
        (post-join-fn (assoc this :child child* :stash (yield child*))))))
  (dot-shape [_] "rect")
  (dot-label [_]
    (let [n (str (:name properties))]
      (if (seq n) (str "Decorator: " n) "Decorator")))
  (dot-color [this]
    (get dot-colors (shift this) (get dot-colors READY))))

(defn decorator
  "Decorator is a hybrid node, defining both tick and part/join calls.

  It holds a single child and decides if it should be executed: first tick
  is executed and calls:

      (branch-pred decorator) => true | false

  **branch-pred returns true:** child will be executed.
  Decorator changes its type to composite and signals Ready. During join
  child node is back and Decorator, it immediately runs post-join-fn:

      (post-join-fn decorator) => decorator'

  This function is fed Decorator containing ticked child, will decide final
  shape of the node. If post-join-fn is nil the decorator will just pass
  through flag and stash of the child.

  **branch-pred returns false:** child is not used, own-tick-fn runs:

      (own-tick-fn decorator) => decorator'

  This constructor accepts a seed of:

  * child, a node to be decorated,
  * branch-pred used for branching? decision if first tick, defaut: yes,
  * own-tick-fn used to tick on decorator if branch-pred returned false,
    default: success,
  * post-join-fn used to tick after join of finalized child, default: nil,
  * flag preset, default: READY,
  * properties for the node, default: nil,
  * paramteres for the node, default: nil,
  * stash for the node, default: nil.

  By default Decorator will try to execute the child and pass its status
  up. If forced to tick default own-tick will just succeed.

  Callable expectations, stash and stashless:

      (branch-pred decorator) => true | false

      (post-join-fn decorator) => Success | Failure | Running | Ready

      (own-tick-fn decorator) => Success | Failure | Running | Ready
  "
  ([seed]
   (branches
    (Decorator. nil
                (get seed :branch-pred yes)
                (get seed :own-tick-fn success)
                (get seed :post-join-fn)
                (get seed :branching? true)
                (get seed :flag READY)
                (get seed :properties)
                (get seed :parameters)
                (get seed :stash))
    [(get seed :child)])))

(defn decorate
  "Decorate a child with default, pass-through Decorator."
  [child]
  (decorator {:child child}))

(defrecord Root [tree properties parameters stack]
  Stepper
  (-stepper? [_] true)
  (finished? [_]
    (and (= 0 (count stack)) (decided? tree)))
  (handle-branch [this]
    (if-let [child* (part tree)]
      (assoc this :tree (shift child* READY) :stack (conj stack tree))
      (throw (ex-info "Child not available from tree!"
                      {:root (constantly this)}))))
  (handle-leaf [this]
    (assoc this :tree (run-hooks! (tick tree))))
  (step [this]
    (cond
      (and (done? tree) (= 0 (count stack)))
      this
      (running? tree) ;; shadow step ;-)
      (step (assoc this :tree (shift tree READY)))
      (decided? tree)
      (if (= 0 (count stack))
        this
        (assoc this
               :tree  (run-hooks! (join (peek stack) tree))
               :stack (pop stack)))
      (branch? tree)
      (handle-branch this)
      :else
      (handle-leaf this)))
  (step-until [this pred]
    (loop [this* (step this)]
      (if (pred this*)
        this*
        (recur (step this*)))))
  (callstack [_]
    (conj (vec stack) tree))
  (exhaust [this]
    (cond
      (and (done? tree) (= 0 (count stack)))
      this
      (running? tree) ;; shadow step ;-)
      (exhaust (assoc this :tree (shift tree READY)))
      :else
      (loop [local-tree tree local-stack stack]
        (cond
          (decided? local-tree)
          (if (= 0 (count local-stack))
            (assoc this :tree local-tree :stack [])
            (recur (run-hooks! (join (peek local-stack) local-tree))
                   (pop local-stack)))
          (branch? local-tree)
          (if-let [child (part local-tree)]
            (recur (shift child READY) (conj local-stack local-tree))
            (throw (ex-info "Child not available from tree!"
                            {:root (constantly (assoc this
                                                      :tree  local-tree
                                                      :stack local-stack))})))
          :else
          (recur (run-hooks! (tick local-tree)) local-stack)))))
  Node
  (-node? [_] true)
  (label [_] (:name properties ""))
  (label [this label*] (assoc-in this [:properties :name] label*))
  (hooks [_] (hooks tree))
  (hooks [this hooks*] (assoc this :tree (hooks tree hooks*)))
  (props [_] properties)
  (props [this props*] (assoc this :properties props*))
  (params [_] parameters)
  (params [this params*] (assoc this :parameters params*))
  (branches [_] [tree])
  (branches [this children*] (assoc this :tree (first children*)))
  (shift [_] (shift tree))
  (shift [this flag*] (assoc this :tree (shift tree flag*)))
  (stem [this] this)
  (stem [_ stem*] (yield stem* (yield tree)))
  (yield [_] (yield tree))
  (yield [this yield*] (assoc this :tree (yield tree yield*)))
  (branch? [_] false)
  (tick [this] (exhaust this))
  (dot-shape [_] "diamond")
  (dot-label [_]
    (let [n (str (:name properties))]
      (if (seq n) (str "Root: " n) "Root")))
  (dot-color [this]
    (get dot-colors (shift this) (get dot-colors READY))))

(defn root
  "Create Root node with given seed and tree."
  ([seed]
   (Root. (get seed :tree)
          (get seed :properties)
          (get seed :parameters)
          (get seed :stack []))))

(defn root-of
  "Create Root node with given tree node."
  [tree]
  (root {:tree tree}))

;; Extra Nodes

(defrecord Robin [children cids retry rewind-fn summary-fn retry-flag cycle-flag
                  flag properties parameters stash]
  Node
  (-node? [_] true)
  (shift [_] flag)
  (shift [this flag*] (assoc this :flag flag*))
  (label [_] (:name properties ""))
  (label [this label*] (assoc-in this [:properties :name] label*))
  (hooks [_] (:hooks properties))
  (hooks [this hooks*] (assoc-in this [:properties :hooks] hooks*))
  (props [_] properties)
  (props [this props*] (assoc this :properties props*))
  (params [_] parameters)
  (params [this params*] (assoc this :parameters params*))
  (branches [_] children)
  (branches [this children*]
    (let [nodes (vec children*)
          cids  (-> (queue (filter (comp not done? (partial get nodes))
                                   (range (count nodes))))
                    (conj -1))]
      (assoc this :children (vary-meta nodes assoc :initial nodes) :cids cids)))
  (stem [this] this)
  (stem [_ stem*] (yield stem* stash))
  (yield [_] stash)
  (yield [this yield*] (assoc this :stash yield*))
  (branch? [_] true)
  (part [_]
    (yield (get children (peek cids)) stash))
  (join [this child*]
    (if (done? this)
      this
      (let [children* (assoc children (peek cids) child*)
            cids*     (if (running? child*)
                        (conj (pop cids) (peek cids))
                        (pop cids))
            this*     (assoc this :children children*
                             :cids cids*
                             :stash (yield child*))
            next*     (peek cids*)]
        (if (not= -1 next*) ;; not end of cycle
          (assoc this* :flag READY)
          (if (not= 1 (count cids*)) ;; not all children done
            (assoc this* :flag cycle-flag :cids (conj (pop cids*) next*))
            (if (nil? summary-fn)
              (assoc this* :flag SUCCESS)
              (let [this** (summary-fn this*)]
                (if (done? this**)
                  this**
                  (let [this*** (assoc this** :flag retry-flag)]
                    (if (done? this***)
                      this***
                      (if (nil? rewind-fn)
                        (branches (update this*** :retry inc)
                                  (:initial (meta children)))
                        (rewind-fn (update this*** :retry inc)))))))))))))
  (dot-shape [_] "rect")
  (dot-label [_]
    (let [n (str (:name properties))
          l (str "Robin (R:" retry ")")]
      (if (seq n)
        (str l ": " n) l)))
  (dot-color [this]
    (get dot-colors (shift this) (get dot-colors READY))))

(defn robin
  "Create a Robin node with given seed and nodes. Children will run in order,
  child that is RUNNING will be put in queue and Robin will get READY
  to execute next child immediately. Robin will be READY on retry and cycle
  completion.

  When all children are either SUCCESS or FAILURE summary-fn will execute
  on Robin. If it returns SUCCESS or FAILURE then Robin ends its run with
  that. If it returns RUNNING Robin will retry.

  Robin will evalute to:

  * SUCCESS if all children are done and either summary-fn sets SUCCESS
    or summary-fn is nil
  * FAILURE if all children are done and summary-fn sets FAILURE
  * cycle-flag on cycle completion, READY by default
  * retry-flag on retry initiation, READY by default.

  The rewind-fn is given Robin to prepare it for re-run, defaults to nil,
  in which case fresh copy of children is used. The rewind-fn is able
  to overwrite retry-flag, because that is set before it runs.

  Robin is a non-canon Parallel implementation."
  ([seed]
   (branches (Robin. []
                     nil
                     (get seed :retry 0)
                     (get seed :rewind-fn)
                     (get seed :summary-fn)
                     (get seed :retry-flag READY)
                     (get seed :cycle-flag READY)
                     (get seed :flag READY)
                     (get seed :properties)
                     (get seed :parameters)
                     (get seed :stash))
             (get seed :children))))

(defn as-robin
  "Create a Robin from collection of nodes."
  [coll]
  (robin {:children coll}))

(defn robin*
  "Create a Robin from node arguments."
  [& nodes]
  (robin {:children nodes}))

;; Predefined placeholder nodes

(def success-action
  "Predefined Action that will succeed on tick."
  (fn->action success))

(def failure-action
  "Predefined Action that will fail on tick."
  (fn->action failure))

(def running-action
  "Predefined Action that will be running on tick."
  (fn->action running))

(def success-condition
  "Predefined succeeding condition."
  (fn->condition yes))

(def failure-condition
  "Predefined failing condition."
  (fn->condition no))

;; Predefined helpers

(defn to-success
  "Create an Action that runs given function on stash and succeeds."
  [a-fn]
  (action
   {:properties {:name (str "to-success: " (friendlify a-fn))
                 :a-fn a-fn}
    :tick-fn    (fn to-success-tick-fn
                  [self]
                  (let [f (get-in self [:properties :a-fn] identity)]
                    (success self (f (yield self)))))}))

(defn to-failure
  "Create an Action that runs given function on stash and fails."
  [a-fn]
  (action
   {:properties {:name (str "to-failure: " (friendlify a-fn))
                 :a-fn a-fn}
    :tick-fn    (fn to-failure-tick-fn
                  [self]
                  (let [f (get-in self [:properties :a-fn] identity)]
                    (failure self (f (yield self)))))}))

(defn via-pred
  "Create a Condition that runs given predicate on stash."
  [pred]
  (condition
   {:properties {:name (str "via-pred: " (friendlify pred))
                 :pred pred}
    :pred-fn    (fn via-predicate-pred-fn
                  [self]
                  (let [p (get-in self [:properties :pred] no)]
                    (p (yield self))))}))

;; DOTs

(defn dot-string
  "Eats node, returns DOT graph string.

  Walks the tree Depth-First, numbers nodes and links them.
  "
  [node]
  (letfn [(style [a-node]
            (str "[shape="    (dot-shape a-node)
                 ", color="   (dot-color a-node)
                 ", label=\"" (dot-label a-node) "\"]"))
          (node-id [n] (str "node" n))
          (pairs [parent cs]
            (when (seq cs)
              (mapv #(assoc % :p-node parent) cs)))]
    (loop [idx 0 log [] curr node more []]
      (if curr
        (let [n (node-id idx)
              p (:p-node curr)
              m (concat more (pairs n (branches curr)))]
          (recur (inc idx)
                 (into log [(str n " " (style curr))
                            (when p
                              (str p " -- " n
                                   "[color=" (dot-color curr)
                                   ", headlabel=" (name (shift curr))
                                   "]"))])
                 (first m)
                 (rest m)))
        (string/join
         \newline
         (concat
          [(str "graph \"" (dot-label node) "\" {")
           "rankdir=TD; splines=true;"
           "graph [pad=\"1\", ranksep=\"0.5\", nodesep=\"0.75\"];"
           "edge [fontname=\"monotype\", labelfontsize=\"10\"];"
           "node [fontname=\"monotype\", fontsize=\"11\", margin=\"0.1\"];"]
          log
          ["}" ""]))))))

#?(:clj
   (defn ->dot
     "Eats node, saves tree to DOT file. (Uses spit -- Clojure only.)"
     [node file-name]
     (io! "Spitting to a file!"
          (spit file-name (dot-string node))
          true)))

#?(:clj
   (defn ->svg
     "Eats node, saves tree to SVG. (Shells out to dot -- Clojure only.)"
     [node file-name]
     (io! "Shelling out to clojure.java.shell/sh"
          (-> (sh "dot" "-Tsvg" (str "-o" file-name) :in (dot-string node))
              :exit
              zero?))))

#?(:clj
   (defn ->png
     "Eats node, saves tree to PNG. (Shells out to dot -- Clojure only.)"
     [node file-name]
     (io! "Shelling out to clojure.java.shell/sh"
          (-> (sh "dot" "-Tpng" (str "-o" file-name) :in (dot-string node))
              :exit
              zero?))))
