# Contributors

## **[Arron Mabrey](https://bitbucket.org/arronmabrey/)**

ClojureScript compatibility in 0.4.0: porting project to feature expressions,
setting up Nashorn and Node.js based CLJS testing, porting entire test suite
from Midje to `clojure.test` and `cljs.test`.
