# wikla

Behavior Tree in Clojure(Script)

[![wikla](https://clojars.org/wikla/latest-version.svg)](https://clojars.org/wikla)

| [Architecture][architecture]
| [API codox][latest]
| [Changelog][changelog]
| [Breakage][breakage]
| [Contributors][contributors]

## What is this?

Data oriented, deterministic, functional (immutability & purity) implementation
of Behavior Tree algorithm, in the AI, robotics and control take.

Inspired by sections I-IV of [Towards a Unified Behavior Trees Framework for
Robot Control by Marzinotto et al. (2014)][2013-ICRA-mcko] ([mirror][2013-ICRA-mcko-mirror]).

## Usage

Take a peek [Architecture document][architecture] and [code examples][examples].

Graphical trees export can be done with the awesome [Graphviz][graphviz]
software. JVM version provides direct export functions to dot, PNG and SVG.

## Development and Testing

Using [Leiningen][leiningen]. Tested with `clojure.test` and `cljs.test`
with `lein-doo`, aliases provided.

Clojure compatibility:

* Up to wikla 0.6.8: Clojure 1.7.0+ (clj/cljc reader conditionals)
* From wikla 0.6.9: Clojure 1.8.0+ (due to clojure.lang.Tuple)
* From wikla 0.7.0: Clojure 1.9.0+ (due to clojure.spec.alpha)

ClojureScript testing relies on [nodejs][nodejs].

Test with:

```bash
# just Clojure
lein test-clj

# just ClojureScript
lein test-cljs

# both Clojure and ClojureScript via Node.js
lein test-all
```

## License

Copyright © 2014-2024 Sławek Gwizdowski

License under **MIT Public License**, see **LICENSE** file in repository.

[2013-ICRA-mcko]: http://www.csc.kth.se/~miccol/Michele_Colledanchise/Publications_files/2013_ICRA_mcko.pdf
[2013-ICRA-mcko-mirror]: https://spottr.bitbucket.io/wikla/2013_ICRA_mcko.pdf
[architecture]: ARCHITECTURE.md
[latest]: https://spottr.bitbucket.io/wikla/latest/
[examples]: examples/
[graphviz]: http://www.graphviz.org/
[nodejs]: https://nodejs.org/
[changelog]: CHANGELOG.md
[breakage]: BREAKAGE.md
[contributors]: CONTRIBUTORS.md
[leiningen]: https://leiningen.org/
[automation]: https://www.xkcd.com/1319/
